<%@page contentType="text/html; charset=UTF-8"%>
<%@page import="org.json.simple.JSONArray"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="db.DBManager" %>
<%@page import="db.TimeStamp" %>
<%@page import="java.util.*" %>
<%@page import="precompute.InitState" %>
<%@page import="precompute.InitStateME" %>
<%@page import="precompute.StateUpdate" %>
<%@page import="precompute.StateUpdateME" %>
<%@page import="precompute.TweetsOfNetwork" %>
<%@page import="precompute.AuthorInfo" %>
<%
	DBManager dbm = new DBManager();
	Integer rumor_type = Integer.parseInt(request.getParameter("RID"));			// 0 - meteor, 1 - AP

	ArrayList<StateUpdateME> PassiveMEStateUpdate = dbm.getStateUpdateME(rumor_type, false);
	ArrayList<StateUpdateME> ActiveMEStateUpdate = dbm.getStateUpdateME(rumor_type, true);
	
	ArrayList<TweetsOfNetwork> TweetNet = dbm.getTweetNetwork(rumor_type);
	ArrayList<TimeStamp> ts = dbm.selectTweetTimeline(-1, -1, rumor_type);
	HashMap<String, AuthorInfo> AuthorInfoMap = dbm.selectAllAuthorInfo(rumor_type);
	HashMap<Long,String> EmbedHtml = dbm.selectAllEmbed(rumor_type);
	
	JSONArray ret=new JSONArray();
	System.out.println(ts.size());
	for(int i = 0; i < ts.size(); i++){
		JSONObject TweetState = new JSONObject();
		// getTweet ID

		TweetState.put("Date", new Long(ts.get(i).date));
		
		TweetState.put("TweetOfFollowers", TweetNet.get(i).TweetsOfFollowers);
		TweetState.put("TweetOfFollowees", TweetNet.get(i).TweetsOfFollowees);
		TweetState.put("TweetOfAuthors", TweetNet.get(i).TweetsOfAuthors);
		TweetState.put("Type", new Integer(ts.get(i).type));
		
		if(EmbedHtml.get(ts.get(i).tweet_id) != null){
			TweetState.put("Tweet", EmbedHtml.get(ts.get(i).tweet_id));
		} else {
			TweetState.put("Tweet", "");
		}
		
		if(rumor_type == 0){
			TweetState.put("Author", ts.get(i).Author);
		} else {
			TweetState.put("Author", AuthorInfoMap.get(Long.toString(ts.get(i).Author_ID)).ScreenName);
		}

		TweetState.put("TweetID", new Long(ts.get(i).tweet_id).toString());
		
		if((rumor_type == 0 && AuthorInfoMap.get(ts.get(i).Author) != null) ||
			(rumor_type == 1 && AuthorInfoMap.get(Long.toString(ts.get(i).Author_ID)) != null)	){
			String key;
			if(rumor_type == 0) 
				key = ts.get(i).Author;
			else 
				key = Long.toString(ts.get(i).Author_ID);
			TweetState.put("Author_Follower", new Integer(AuthorInfoMap.get(key).followers));
			TweetState.put("Author_Followee", new Integer(AuthorInfoMap.get(key).followees));
			TweetState.put("Author_RumorExp", new Integer(AuthorInfoMap.get(key).RECount));
			TweetState.put("Author_DebunkExp", new Integer(AuthorInfoMap.get(key).DECount));
			TweetState.put("Author_RumorTweet", new Integer(AuthorInfoMap.get(key).TRCount));
			TweetState.put("Author_DebunkTweet", new Integer(AuthorInfoMap.get(key).TDCount));
		} else {
			TweetState.put("Author_Follower", new Integer(-1));
			TweetState.put("Author_Followee", new Integer(-1));
			TweetState.put("Author_RumorExp", new Integer(-1));
			TweetState.put("Author_DebunkExp", new Integer(-1));
			TweetState.put("Author_RumorTweet", new Integer(-1));
			TweetState.put("Author_DebunkTweet", new Integer(-1));
		}
		
		for(int j = 0; j < StateUpdateME.transitionNames.length; j++){				// Put InitStates info
			TweetState.put("P" + StateUpdateME.transitionNames[j], new Integer(PassiveMEStateUpdate.get(i).transitions[j]));
			TweetState.put("A" + StateUpdateME.transitionNames[j], new Integer(ActiveMEStateUpdate.get(i).transitions[j]));
//			System.out.print(ActiveMEStateUpdate.get(i).transitions[j] + ",");
		}
//		System.out.println();
		ret.add(TweetState);
	}
//	System.exit(0);
    out.print(ret);
    out.flush();
%>