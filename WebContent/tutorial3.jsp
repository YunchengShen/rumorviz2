<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>State Diagram </title>
    <link rel="stylesheet" href="style/base.css">
        <link rel="stylesheet" href="style/app.css">
    <link rel="stylesheet" href="style/sankey.css">
	<script src="http://d3js.org/d3.v3.min.js"></script>
	
	<script type="text/javascript" src="scripts/randomData.js"></script>
	<script src="scripts/dataModule.js"></script>
	<script src="scripts/sankey.js"></script>
</head>
<body onmouseup="triggerMagUpdate()" onkeydown="movePointer()">
	<div id="main">
		<div id="quiz">
			<div class="qheader"> <p class="qheader">Exercise) Find the person who made the first tweet on Apr. 24. How many times did this person propagate the rumor?</p></div>
			<div class="qselections">
				<form action=""><input type="radio" value="a" name="question" onclick="checkQuiz3('a')">a) 1 <input type="radio" value="b" name="question" onclick="checkQuiz3('b')">b) 3 <input type="radio" value="c" name="question" onclick="checkQuiz3('c')">c) 5 <input type="radio" value="d" name="question" onclick="checkQuiz3('d')">d) 7<br></form>
			</div>
            <span id="explanation"></span>
            <button id="proceed" type="button" onclick="parent.location.href='/rumorlens/tutorial4.html'" style="visibility:hidden">Next</button>
		</div>
		<div id="SankeyMode" style="position:absolute; left:740px; top:125px">
			    <form action="">
					<input id="PMode" type="radio" name="sm" onclick="changeSankeyMode('Exposed')" checked>Exposed
					<input id="AMode" type="radio" name="sm" onclick="changeSankeyMode('Tweeted')">Tweeted
				</form>	
		</div>
		<div id="barzone">
			<div class="ItemInfo" id="TweetInfo"></div>
			<table class="ItemInfo" id="AuthorInfo">
				<thead><tr><th scope="col">Author</th></tr></thead>
				<tbody><tr><td><p>  </p></td></tr></tbody>
			</table>
			<table class="ItemInfo" id="TimeInfo">
				<thead><tr><th scope="col">Time</th></tr></thead>
				<tbody><tr><td><p>  </p></td></tr></tbody>
			</table>
		</div>
	</div>
	<div id="TimelineZone" style="position:absolute; left:0px; top:685px">
		<p id="pc_label">Cursor Control</p>
    	<div id="pointer_control" style="visibility:visible">
			<button id="leftButton" type="button" onclick="movePointerToLeft()"><</button>
			<button id="rightButton" type="button" onclick="movePointerToRight()">></button>
            <button id="restoreButton" type="button" onclick="restoreCursor()" disabled>Restore Cursor</button>
		</div>
		<div id="delete_window" style="visibility:hidden">
			<button id="delWindow" type="button" onclick="deleteWindow()">X</button>		
		</div>
	</div>
    <script src="scripts/app2_1.js"></script>
    <script src="scripts/TimeControl.js"></script>
    <script src="scripts/tweetdisplay.js"></script>
    <script src="scripts/quizCheck.js"></script>
<!--     <script src="BarChart.js"></script>	 -->
</body>
</html>
