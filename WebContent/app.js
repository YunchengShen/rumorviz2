var radius = 40;

// variables for state diagram
window.states = [
    { x : 50, y : 270, label : "S", transitions : [] },
    { x : 350, y : 190, label : "Rumor Exp", transitions : [] },
    { x : 350, y : 340, label : "Debunk Exp", transitions : [] },
    { x : 650, y : 270, label : "Both Exp", transitions : [] },
    { x : 650, y : 10, label : "Tweet Rumor", transitions : [] },
    { x : 650, y : 540, label : "Tweet Debunk", transitions : [] },
    { x : 850, y : 270, label : "Tweet Both", transitions : [] }
];

window.states[0].transitions.push( { target : window.states[ 1]});
window.states[0].transitions.push( { target : window.states[ 2]});
window.states[0].transitions.push( { target : window.states[ 4]});
window.states[0].transitions.push( { target : window.states[ 5]});
window.states[1].transitions.push( { target : window.states[ 3]});
window.states[1].transitions.push( { target : window.states[ 4]});
window.states[1].transitions.push( { target : window.states[ 5]});
window.states[2].transitions.push( { target : window.states[ 3]});
window.states[2].transitions.push( { target : window.states[ 4]});
window.states[2].transitions.push( { target : window.states[ 5]});
window.states[3].transitions.push( { target : window.states[ 4]});
window.states[3].transitions.push( { target : window.states[ 5]});
window.states[4].transitions.push( { target : window.states[ 6]});
window.states[5].transitions.push( { target : window.states[ 6]});

window.weights = [
   { x : 250, y : 240, label : "S-Rexp", rlabel: "S-Rexp"},
   { x : 250, y : 310, label : "S-Dexp", rlabel: "S-Dexp"},
   { x : 170, y : 190, label : "S-RT", rlabel: "S-RT"},
   { x : 170, y : 370, label : "S-DT", rlabel: "S-DT"},
   { x : 550, y : 240, label : "Rexp-BExp", rlabel: "Rexp-BExp"},
   { x : 400, y : 140, label : "Rexp-RT", rlabel: "Rexp-RT"},
   { x : 500, y : 410, label : "Rexp-DT", rlabel: "Rexp-DT"},
   { x : 550, y : 310, label : "Dexp-BExp", rlabel: "Dexp-BExp"},
   { x : 500, y : 140, label : "Dexp-RT", rlabel: "Dexp-RT"},
   { x : 400, y : 410, label : "Dexp-DT", rlabel: "Dexp-DT"},
   { x : 650, y : 170, label : "BExp-RT", rlabel: "BExp-RT"},
   { x : 650, y : 410, label : "BExp-DT", rlabel: "BExp-DT"},
   { x : 750, y : 120, label : "RT-BT", rlabel: "RT-BT"},
   { x : 750, y : 440, label : "DT-BT", rlabel: "DT-BT"},
   { x : 350, y : 250, label : "RExp-RExp", rlabel: "RExp-RExp"},
   { x : 350, y : 290, label : "DExp-DExp", rlabel: "DExp-DExp"},
   { x : 690, y : 300, label : "BExp-BExp", rlabel: "BExp-BExp"},
   { x : 720, y : 40, label : "TR-TR", rlabel: "TR-TR"},
   { x : 720, y : 500, label : "TD-TD", rlabel: "TD-TD"},
   { x : 880, y : 300, label : "BT-BT", rlabel: "BT-BT"}			
];

reflexives = [];

function getArcDesc(x, y, sa, ea, large_arc, sweep_flag){
	sp = polarToCartesian(x,y, 15, sa);
	ep = polarToCartesian(x,y, 15, ea);
	return "M" + sp[0] + ' ' + sp[1] + ' A 15 15 0 ' + large_arc + ' ' + sweep_flag + ' ' + ep[0] + ' ' + ep[1];
};

function polarToCartesian(centerX, centerY, radius, angleInDegrees) {
	  var angleInRadians = angleInDegrees * Math.PI / 180.0;
	  var x = centerX + radius * Math.cos(angleInRadians);
	  var y = centerY + radius * Math.sin(angleInRadians);
	  return [x,y];
};

reflexives.push( { label : "RE", d : getArcDesc(340,225, 225, 315, 1, 0) } );
reflexives.push( { label : "DE", d : getArcDesc(340,305,135,45, 1, 1)  }) ;
reflexives.push( { label : "BE", d : getArcDesc(695,270,225,135, 1, 1) } );
reflexives.push( { label : "TR", d : getArcDesc(700,10, 225,135, 1, 1) } );
reflexives.push( { label : "TD", d : getArcDesc(700,540,225,135, 1, 1) } );
reflexives.push( { label : "TB", d : getArcDesc(880,270,225,135, 1, 1) } );

window.svg = d3.select( '#main')
.append("svg")
//.attr("viewBox", "0 0 " + 1000 + " " + 1000 )
//.attr("preserveAspectRatio", "xMinYMin")
.attr("width", "960px")
.attr("height", "560px")
.attr('class', 'diagram');

//define arrow markers for graph links
marker_id = [ 'end-arrow0', 'end-arrow1', 'end-arrow2', 'end-arrow3', 'end-arrow4', 'end-arrow5', 'end-arrow6',  
              'end-arrow7', 'end-arrow8', 'end-arrow9', 'end-arrow10', 'end-arrow11', 'end-arrow12', 'end-arrow13',
              'end-arrow14', 'end-arrow15', 'end-arrow16', 'end-arrow17', 'end-arrow18', 'end-arrow19'];		// reflexive arrows from 14

path_id = [ 'ap-0', 'ap-1', 'ap-2', 'ap-3', 'ap-4', 'ap-5', 'ap-6', 'ap-7', 'ap-8', 
            'ap-9', 'ap-10', 'ap-11', 'ap-12', 'ap-13',
            'ap-14', 'ap-15', 'ap-16', 'ap-17', 'ap-18', 'ap-19'];				// reflexive links from 14

path_names = ['S-RE', 'S-DE', 'S-TR', 'S-TD', 'RE-BE', 'RE-TR', 'RE-TD', 'DE-BE', 'DE-TR', 
              'DE-TD', 'BE-TR', 'BE-TD', 'TR-TB', 'TD-TB',
              'RE-RE', 'DE-DE', 'BE-BE', 'TR-TR', 'TD-TD', 'TB-TB'];			// reflexive links from 14

//path_colors = ['#98abc5', 'Blue', 'Fuchsia', '#d0743c', 'Green', 'Lime', 'Maroon', 'Yellow', 'Olive', 'Purple', 'Red', 
//               'Silver', 'Teal', '#6b486b']; 
path_colors = ['#98abc5', '#8a89a6', 'Fuchsia', '#d0743c', '#d0743c', 'Lime', 'Maroon', '#6b486b', 'Olive', 'Purple', 'Red', 
               'Silver', 'Teal', '#6b486b',
               'Coral', 'DarkKhaki', 'DarkSlateBlue', 'FireBrick', 'DarkMagenta', 'BlueViolet']; // reflexive links from 14

svg.append('svg:defs');

d3.select('defs').selectAll('marker')
	.data(marker_id).enter().append('marker')
	.attr('id', function (d) { return d; })
    .attr('viewBox', '0 -5 10 10')
    .attr('refX', 3)
    .attr('markerWidth', 4)
    .attr('markerHeight', 4)
    .attr('orient', 'auto')
    .append('svg:path')
    .attr('d', 'M0,-5L10,0L0,5')
    .attr('fill', '#000')
;

var marker_paths = d3.selectAll('path').data(path_id)
	.attr('id', function (d) { return d; });
marker_paths.enter().append('path');
marker_paths.exit().remove();

var gStates = svg
    .selectAll( "g.state")
    .data( states);

var transitions = function() {
    return states.reduce( function( initial, state) {
        return initial.concat( 
            state.transitions.map( function( transition) {
                return { source : state, target : transition.target, label : "test"};
            })
        );
    }, []);
};
    // http://www.dashingd3js.com/svg-paths-and-d3js
var computeTransitionPath = /*d3.svg.diagonal.radial()*/function( d) {
    var deltaX = d.target.x - d.source.x,
    deltaY = d.target.y - d.source.y;
    
    dist = Math.sqrt(deltaX * deltaX + deltaY * deltaY),
    normX = deltaX / dist,
    normY = deltaY / dist,
    sourcePadding = radius + 2;//d.left ? 17 : 12,
    targetPadding = radius + 6;//d.right ? 17 : 12,
    sourceX = d.source.x + (sourcePadding * normX),
    sourceY = d.source.y + (sourcePadding * normY),
    targetX = d.target.x - (targetPadding * normX),
    targetY = d.target.y - (targetPadding * normY);
    return 'M' + sourceX + ',' + sourceY + 'L' + targetX + ',' + targetY;
};

var gTransitions = svg.append( 'g')  
    .selectAll( "path.transition")
    .data( transitions)
    .attr('id', function (d, i){ return path_id[i]; })
;

var gBackground = svg.append( 'g')
	.selectAll( "path.background")
	.data( transitions)
;

var gReflexives = svg.append('g')
	.selectAll("path.reflexive")
	.data(reflexives)
;

var gRBackground = svg.append('g')
	.selectAll("path.rbackground")
	.data(reflexives)
;

var startState, endState;    

var margin = {top: 20, right: 15, bottom: 118, left: 60}
, width = 960 - margin.left - margin.right
//, miniHeight = lanes.length * 12 + 50
, miniHeight = 1 * 12 + 50;
var m_mini_height = 30;
var chart = d3.select('#main')
.append('svg')
.attr('width', width + margin.right + margin.left)
.attr('height', miniHeight + margin.bottom + m_mini_height + margin.top)
.attr('class', 'chart');

var m_mini = chart.append('g')
.attr('transform', 'translate(' + margin.left + ',' + 15 + ')')
//.attr('width', width)
.attr('width', width + margin.right + margin.left)
.attr('height', m_mini_height + margin.top)
.attr('class', 'm_mini');

var mini = chart.append('g')
.attr('transform', 'translate(' + margin.left + ',' + 65 + ')')
//.attr('width', width)
.attr('width', width + margin.right + margin.left)
.attr('height', miniHeight)
.attr('class', 'mini');

var TweetInfoArea = chart.append('g')
.attr('transform', 'translate(' + 0 + ',' + 55 + ')')
//.attr('width', width)
.attr('width', width + margin.right + margin.left)
.attr('height', margin.bottom)
.attr('class', 't_info');

mini.selectAll('rect.background').remove();

var bars = [];

function restart() {
    gStates = gStates.data( states);
    
    var gState = gStates.enter()
        .append( "g")
        .attr({
            "transform" : function( d) {
                return "translate("+ [d.x,d.y] + ")";
            },
            'class'     : 'state' 
        });

    gState.append( "circle")
    	.attr("r", function (){ return radius;})
    	.attr({
            class   : 'inner'
        });

    gState.append( "text")
        .attr({
            'text-anchor'   : 'middle',
            y               : 4
        })
        .text( function( d) {
            return d.label;
        })
    ;

    gState.append( "title")
        .text( function( d) {
            return d.label;
        })
    ;
    
    gStates.exit().remove();

    gTransitions = gTransitions.data( transitions);
    gTransitions.enter().append( 'path')
    	.attr( 'class', 'transition')
    	.attr( 'id', function (d, i) { return path_id[i]; })
        .attr( 'd', computeTransitionPath)
        .attr( 'stroke', '#000')
        .attr('marker-end', function (d, i) { return 'url(#end-arrow' + i + ')'; });
    gTransitions.exit().remove();
    
    gBackground = gBackground.data( transitions);
    gBackground.enter().append( 'path')
        .attr( 'class', 'background')
        .attr( 'd', computeTransitionPath)
        .attr( 'stroke', '#000')
        .attr( 'stroke-width', "7px")
        .attr( 'opacity', '0')
        .on("click", function (d, i){
        	var isClicked = d3.select(this).classed('selected');
        	
        	if(isClicked == false) {
	        	d3.select( 'path.transition#' + path_id[i] ).attr( 'stroke', path_colors[i]);
	        	d3.select('path[id=\"ap-' + i + '\"]').attr('fill', path_colors[i]);
	        	bars.push( { id: i, name: path_names[i], frequency: weights[i].label, r_name: path_names[i], r_frequency: weights[i].rlabel} );
        	} else {
	        	d3.select( 'path.transition#' + path_id[i] ).attr( 'stroke', 'black');
	        	d3.select('path[id=\"ap-' + i + '\"]').attr('fill', 'black');
	        	bars = bars.filter( function (val) { return val.id != i; });
        	}
    	
        	if(bars.length > 0) {
        		drawChart(bars);
           		d3.select('#barzone').style("display", "block");
        	} else if(bars.length == 0){
        		d3.select('#barzone').style("display", "none");
        	}
        	d3.select( this).classed( "selected", !isClicked);
        });
    gBackground.exit().remove();		

    gReflexives = gReflexives.data(reflexives);
    gReflexives.enter().append( 'path')
    	.attr('class', 'reflexive')
    	.attr( 'id', function (d, i) { return path_id[i+14]; })
    	.attr('d', function (item) { return item.d; })
        .attr( 'stroke', '#000')
        .attr('marker-end', function (d, i) { return 'url(#end-arrow' + (i+14) + ')'; });
    gReflexives.exit().remove();
    
    gRBackground = gRBackground.data( reflexives);
    gRBackground.enter().append( 'path')
     	.attr('class', 'rbackground')
    	.attr('d', function (item) { return item.d; })
        .attr( 'stroke', '#000')
        .attr( 'stroke-width', "7px")
        .attr( 'opacity', '0')
        .on("click", function (d, i){
        	var isClicked = d3.select(this).classed('selected');
        	n_i = i+14;
        	if(isClicked == false) {
	        	d3.select( 'path.reflexive#' + path_id[n_i] ).attr( 'stroke', path_colors[n_i]);
	        	d3.select('path[id=\"ap-' + n_i + '\"]').attr('fill', path_colors[n_i]);
	        	bars.push( { id: n_i, name: path_names[n_i], frequency: weights[n_i].label, r_name: path_names[n_i], r_frequency: weights[n_i].rlabel} );
        	} else {
	        	d3.select( 'path.reflexive#' + path_id[n_i] ).attr( 'stroke', 'black');
	        	d3.select('path[id=\"ap-' + n_i + '\"]').attr('fill', 'black');
	        	bars = bars.filter( function (val) { return val.id != n_i; });
        	}
    	
        	if(bars.length > 0) {
        		drawChart(bars);
           		d3.select('#barzone').style("display", "block");
        	} else if(bars.length == 0){
        		d3.select('#barzone').style("display", "none");
        	}
        	d3.select( this).classed( "selected", !isClicked);
        });
    gRBackground.exit().remove();

//    d3.select('.diagram').selectAll('g.weight').data(weights).text(function (d) { return d.label; });
    var gWeights = d3.select('.diagram').selectAll('g.weight').data( weights);

    gWeights.enter()
    	.append("g")
    	.attr({
    		'class'	:	'weight'
    	})
    ;

    gWeights.append( "text")
    	.attr({
    		'x'		: function (d) { return d.x; },
    		'y'		: function (d) { return d.y; },
    		'class'	:	'weight'
    	})
    	.text( function (d) { return d.label; })
    	.on("click", function (d, i){
    		if(i == 2 || i == 3 || i == 5 || i == 6 || i == 8 || i == 9 || i == 10 
    				|| i == 11 || i == 12 || i == 13 || i == 17 || i == 18 || i == 19){
    			highlightItemOfEdge(i);
    		}
        })
    ;

    gWeights.exit().remove();
};

//dataRequest();

