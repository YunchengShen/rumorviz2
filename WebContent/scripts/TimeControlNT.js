
var x, ext, y2, brush, mag_x = null, mag_brush = null, m_y2;
//var bars = [];
var timebar_item_data, mag_items, bar_items, mag_bar_items;
//variables for timebar
var CursorStack = new Array();
function dummy(){
	timebar_item_data = randomData(TweetInfo);
	var lanes = timebar_item_data.lanes,
		items = timebar_item_data.items,
		now = new Date(2013, 2, 20, 0, 0, 0, 0);
	
//	alert('items downloaded: ' + TweetInfo.length + ', in timeline: ' + items.length);
	x = d3.time.scale()
		.domain([d3.min(items, function(d) { return d.start; }),
				 d3.max(items, function(d) { return d.end; })])
		.range([0, width]);
	
	ext = d3.extent(lanes, function(d) { return d.id; });
	y2 = d3.scale.linear().domain([ext[0], ext[1] + 1]).range([0, miniHeight]);
	
	mini.append('g').selectAll('.laneText')
	.data(lanes)
	.enter().append('text')
	.text(function(d) { return d.label; })
	.attr('x', -10)
	.attr('y', function(d) { return y2(d.id + .5); })
	.attr('dy', '0.5ex')
	.attr('text-anchor', 'end')
	.attr('class', 'Timeline');

	var xDateAxis = d3.svg.axis()
	.scale(x)
	.orient('bottom')
	.ticks(d3.time.days, (x.domain()[1] - x.domain()[0]) > 15552e6 ? 2 : 1)
	.tickFormat(d3.time.format('%b %d, %H:%M'))
	.tickSize(6, 0, 0);
	
	var xMonthAxis = d3.svg.axis()
	.scale(x)
	.orient('top')
	.tickValues([items[0].start])
	.tickFormat('cursor')
	.tickSize(15, 0, 0);
	
	mini.append('g')
	.attr('transform', 'translate(0,' + miniHeight + ')')
	.attr('class', 'axis date')
	.call(xDateAxis);
	 
	mini.append('g')
	.attr('transform', 'translate(0,0.5)')
	.attr('class', 'axis month')
	.call(xMonthAxis)
	.selectAll('text')
		.attr('dx', 22)
		.attr('dy', 12);
	
	bar_items = mini.append('g').selectAll('.miniItems').data(getPaths(items, x, y2));
//	.data(getPaths(items.filter( function (it) { return it.Type == '1'; })))
	bar_items.enter().append('path')
	.attr('class', function(d) { return 'miniItem ' + d.class; })
	.attr('d', function(d) { return d.path; });
	bar_items.exit().remove();
	
	//invisible hit area to move around the selection window
	mini.append('rect')
	.attr('pointer-events', 'painted')
	.attr('width', width)
	.attr('height', miniHeight)
	.attr('visibility', 'hidden')
	.on('mousedown', moveBrush)
	.on('mousemove', function() { displayRelated(true, this);})
	.on('mouseout', cleanHovered);
	
	brush = d3.svg.brush()
	.x(x)
	.extent([d3.time.monday(now),d3.time.saturday.ceil(now)])
	.on("brush", updateBrush);
	
	mini.append('g')
	.attr('class', 'x brush')
	.attr('bar','bottom')
	.call(brush)
	.on("mouseup", function () { update_mag_timebar(false); })				// mouseup event is not fired at the brush element. capture it in a different object, maybe window?
	.on('mousedown', brushdown)
	.on('mousemove', function() { displayRelated(true, this); })
	.on('mouseout', cleanHovered)
	.on('mousewheel', function() { zoomBrush(true, this);})
	.selectAll('rect')
		.attr('y', 1)
		.attr('height', miniHeight - 1);
	

	mini.selectAll('rect.background').remove();
	showMagnifiedTimebar();
	updateCursor(-1);
    CursorStack.push(0);
	showTweetInfoArea();
}

/**
 * 
 * @param mainbar: boolean indicating whether the main bar or the mini bar called this function
 * @param container: reference to the object that called this function
 */
var bar = 0;
var baseDelta = 15000; //Base zoom speed (ms)
var minDist = 1000; //How close the zoom can come to the location of the mouse pointer. 

function zoomBrush(mainbar, container)
{
	d3.event.preventDefault();

	//alert("Point: " + point);
	//alert("Brush extent front: " + brush.extent()[0].getTime()+ " | back: "+brush.extent()[0].getTime());
	//alert((brush.extent()[1]).getTime() +10000000);
	//alert("Zoom");
	
	
	var origin = d3.mouse(container);
	var point;
	var leftExtent;
	var rightExtent;
	var leftLimit;
	var rightLimit;
	//The two scales are x and mag_x
	if(mainbar == true) 
	{
/*		if ( bar != 1)
		{
			alert("bottom");
			bar = 1;
		}*/
		point = (x.invert(origin[0])).getTime(); 
		leftExtent = (brush.extent()[0]).getTime();
		rightExtent = (brush.extent()[1]).getTime();
		leftLimit = x.domain()[0].getTime();
		rightLimit = x.domain()[1].getTime();
	} 
	else 
	{
/*		if ( bar != 2)
		{
			alert("top");
			bar = 2;
		}*/
		point = (mag_x.invert(origin[0])).getTime();
		leftExtent = (mag_brush.extent()[0]).getTime();
		rightExtent = (mag_brush.extent()[1]).getTime();
		leftLimit = x.domain()[0].getTime();
		rightLimit = x.domain()[1].getTime();
	}
	
	var span = rightExtent - leftExtent;
	
	//zoom the brush such that the point the mouse is hovering over stays in the middle
	var newLeftExtent;
	var newRightExtent;
	
/*	if (event.wheelDelta > 0)
	{
		//Zoom in 
*/		newLeftExtent = leftExtent+d3.event.wheelDelta*baseDelta*((point-leftExtent)/span);
		newRightExtent = rightExtent-d3.event.wheelDelta*baseDelta*((rightExtent-point)/span);
/*	}
	else
	{
		//Zoom out
		newLeftExtent = leftExtent+d3.event.wheelDelta*baseDelta*((point-leftExtent)/span);
		newRightExtent = rightExtent-d3.event.wheelDelta*baseDelta*((rightExtent-point)/span);
	}
	*/
	//Make sure the brush isn't going outside the bounds of the time-bar, or collapsing to a point
	if (leftLimit <= (newLeftExtent - minDist) && (point >= (newLeftExtent+minDist)) && (rightLimit >= (newRightExtent+1000)) && (point <= (newRightExtent - 1000)) )
	{	
		brush.extent([newLeftExtent,newRightExtent]);
		updateBrush();
		update_mag_timebar(false);
	}
	
}

/*function zoomTopBrush(e)
{
	zoomBrush(e,1);
}

function zoomBottomBrush(e)
{
	zoomBrush(e,2);
}*/

function displayTweet(){
/*	if(highlight_mode != "none"){
		d3.select('.feedback_msg').text('disable the highlight mode to move the cursor');
		return;
	} 
*/	
	var origin = d3.mouse(this)
	  , point = mag_x.invert(origin[0]), tp = point.getTime();
	
	var x_lb = x.domain()[0].getTime(), x_rb = x.domain()[1].getTime();
	var mag_x_lb = mag_x.domain()[0].getTime(), mag_x_rb = mag_x.domain()[1].getTime();
	var click_range = 2500000 * ((mag_x_rb - mag_x_lb) / (x_rb - x_lb));

	// try left item and right item, check if the Date falls within the click_range, then select as a matching index
	// otherwise just return

	var left_index = search_left_index(tp / 1000);
	var right_index = search_right_index(tp / 1000);
	
	if(TweetInfo[left_index].Date * 1000 > mag_x_lb && Math.abs(tp - TweetInfo[left_index].Date * 1000) < click_range){
		pointer_index = left_index;
	} else if(TweetInfo[right_index].Date * 1000 < mag_x_rb && Math.abs(tp - TweetInfo[right_index].Date * 1000) < click_range){
		pointer_index = right_index;
	}
	updateCursor(pointer_index);
	updateNetworkInfo(pointer_index, false);
	updateMagBarItems();
	
	focusNode();
}

function focusNode(){						// highlight the node pointed by the cursor
	if(SelectedNode != -1) 
		NodeInfo[SelectedNode].focus = false;
	NodeInfo[AuthorOrder[TweetInfo[pointer_index].Author]].focus = true;		// show the selected bubble
	SelectedNode = AuthorOrder[TweetInfo[pointer_index].Author];

	var backup = window_updated;
	window_updated = true;
	restart();
	window_updated = backup;
}

var prev_hovered_item = -1;

function displayRelated(mainbar, container){
	var origin = d3.mouse(container);
	var matching_index = -1;
	var point;	

	if(longItems.length == 0)
		return;
	
	if(mainbar == true) {
		point = x.invert(origin[0]); 
	} else {
		point = mag_x.invert(origin[0]);
	}
	var tp = point.getTime();
	
	var x_lb = x.domain()[0].getTime(), x_rb = x.domain()[1].getTime();
	var mag_x_lb = mag_x.domain()[0].getTime(), mag_x_rb = mag_x.domain()[1].getTime();
	var hover_range;
	if(mainbar == true){
		hover_range = 2500000;	
	} else {
		hover_range = 2500000 * ((mag_x_rb - mag_x_lb) / (x_rb - x_lb));	
	}
	
	for(var i = 0; i < longItems.length; i++){
		var start_in_seconds = timebar_item_data.items[longItems[i]].start.getTime();
		if(tp >= (start_in_seconds - hover_range) && tp <= (start_in_seconds + hover_range)){
			matching_index = longItems[i];
		}
	}
	
	if(matching_index != -1){
		if(matching_index == prev_hovered_item && endsWith(timebar_item_data.items[matching_index].class, "hovered") == true) return;
		
		prev_hovered_item = matching_index;
		cleanHovered();
		if(endsWith(timebar_item_data.items[matching_index].class, "hovered") == false){
			timebar_item_data.items[matching_index].class = timebar_item_data.items[matching_index].class + ' hovered';
			
			updateBarItems();
			updateMagBarItems();
			
			updateTweetInfo(matching_index);
		}
	}
}

function highlightItemOfEdge(EdgeName){
	while(longItems.length > 0){		// initialize items (turn off highlited items)
		var idx_of_tid = longItems.shift();
		var length = timebar_item_data.items[idx_of_tid].class.length;
		timebar_item_data.items[idx_of_tid].class = timebar_item_data.items[idx_of_tid].class.substring(0, length-9);
	}
	
	if(ETTI_Ubound[EdgeName] >= 1){
		for(var it = 0; it < ETTI_Ubound[EdgeName]; it++){
			var idx_of_tid = EdgeToTweetIndex[EdgeName][it];
//			alert(idx_of_tid + ', type: ' + TweetInfo[idx_of_tid].Type + ', but: ' + timebar_item_data.items[idx_of_tid].class);
			timebar_item_data.items[idx_of_tid].class = timebar_item_data.items[idx_of_tid].class + ' longItem';
			longItems.push(idx_of_tid);
		}
	}
	
	updateBarItems();
	updateMagBarItems();
}

function cleanHovered(){
	var clean_flag = false;
	
	if(longItems.length == 0)
		return;
	
	for(var i = 0; i < longItems.length; i++){
		var idx_of_tid = longItems[i];
		if(endsWith(timebar_item_data.items[idx_of_tid].class, "hovered") == true){
			var length = timebar_item_data.items[idx_of_tid].class.length;
			timebar_item_data.items[idx_of_tid].class = timebar_item_data.items[idx_of_tid].class.substring(0, length-8);
			clean_flag = true;
		}
	}
	if(clean_flag == true){
		updateBarItems();
		updateMagBarItems();
	}
	
	updateTweetInfo(pointer_index);
}

var label_printed = false, pointer_index = 0;

function update_mag_timebar(minimize){
	var lanes = timebar_item_data.lanes;

//	TweetInfoArea.select('.debug').text('mag view update - ' + brush.extent()[0].getTime().toString() + ', ' + brush.extent()[1].getTime().toString());		
	var minExtent = d3.time.second(brush.extent()[0])
	  , maxExtent = d3.time.second(brush.extent()[1]);

	if(minimize == true){
		maxExtent = new Date(minExtent.getTime() + milisec_per_pix*2);
	}

	mag_x = d3.time.scale()
		.domain([minExtent,maxExtent])
		.range([0, width]);
	
	m_y2 = d3.scale.linear().domain([ext[0], ext[1] + 1]).range([0, m_mini_height]);
	
	if(label_printed == false) {
		m_mini.append('g').selectAll('.laneText')
		.data(['Magnified'])
		.enter().append('text')
		.text(function(d) { return d; })
		.attr('x', -6)
		.attr('y', 30)
		.attr('dy', '0.5ex')
		.attr('text-anchor', 'end')
		.attr('class', 'Timeline');
		
		label_printed = true;
		
		d3.select("#pointer_control").style('visibility', 'visible');
		d3.select("#delete_window").style('visibility', 'visible');
	}
//	TweetInfoArea.select('.debug').text('mag view update after - ' + minExtent.getTime().toString() + ', ' + maxExtent.getTime().toString());		

	mag_items = timebar_item_data.items.filter(function (val) { return ((val.start >= minExtent) && (val.end <= maxExtent));});

/*
	var left_index = search_left_index(minExtent.getTime() / 1000);
	var right_index = search_right_index(maxExtent.getTime() / 1000);
	
	if(left_index <= right_index){
		updateCursor(left_index);		
	} else {
		updateCursor(-1);		
	}
	
	pointer_index = left_index;
*/

	updateCursor(pointer_index);		
	
	var xDateAxis = d3.svg.axis()
		.scale(mag_x)
		.orient('bottom')
		.ticks(d3.time.days, (mag_x.domain()[1] - mag_x.domain()[0]) > 15552e6 ? 2 : 1)
		.tickFormat(d3.time.format('%b %d, %H:%M'))
		.tickSize(6, 0, 0);

	m_mini.transition().duration(0).select('.m_axis.date')
		.call(xDateAxis).attr('visibility', 'visible');

	mag_brush = mag_brush.x(mag_x);
	m_mini.select('.brush').call(mag_brush.extent([minExtent, maxExtent]));
	
	updateMagBarItems();
	brush_updating = false;
}

function triggerMagUpdate(){
	if(brush_updating == true){
		update_mag_timebar(false);
		brush_updating = false;
	}
}

function updateBarItems(){
	var items = timebar_item_data.items;
	var ItemClasses = getPaths(items, x, y2);

	bar_items = bar_items.data(ItemClasses, function(d) { return d.class + '-' +d.timestamp; });
	bar_items.enter().append('path')
		.attr('class', function(d) { return 'miniItem ' + d.class;})
		.attr('d', function(d) {return d.path; });
	bar_items.exit().remove();
}

function updateMagBarItems(){
	mag_bar_items = mag_bar_items.data(getPaths(mag_items, mag_x, m_y2), function(d) { return d.class + '-' +d.timestamp; });
	mag_bar_items.enter().append('path')
//		.attr('visibility', 'visible')
		.attr('class', function(d) { return 'mag_miniItem ' + d.class;})
		.attr('d', function(d) {return d.path; });
	mag_bar_items.exit().remove();
}

function showMagnifiedTimebar(){
	var lanes = timebar_item_data.lanes, items = timebar_item_data.items, 
		now = new Date(2013, 2, 20, 0, 0, 0, 0);
	
	mag_x = x;
	
	var xDateAxis = d3.svg.axis()
	.scale(x)
	.orient('bottom')
	.ticks(d3.time.days, (x.domain()[1] - x.domain()[0]) > 15552e6 ? 2 : 1)
	.tickFormat(d3.time.format('%b %d, %H:%M'))
	.tickSize(6, 0, 0);
	
	var xMonthAxis = d3.svg.axis()
	.scale(x)
	.orient('top')
	.tickValues([items[0].start])
	.tickFormat(d3.time.format(''))
	.tickSize(0, 0, 0);
	
	m_mini.append('g')
	.attr('transform', 'translate(0,' + m_mini_height + ')')
	.attr('class', 'm_axis date')
	.attr('visibility', 'hidden')
	.call(xDateAxis);
	
	m_mini.append('g')
	.attr('transform', 'translate(0,0.5)')
	.attr('class', 'm_axis month')
	.attr('visibility', 'hidden')
	.call(xMonthAxis)
	.selectAll('text')
		.attr('dx', 0)
		.attr('dy', 0);
	
	mag_bar_items = m_mini.append('g').selectAll('.mag_miniItems')
		.data(getPaths(items, x, y2));
	mag_bar_items.enter().append('path')
	.attr('visibility', 'hidden')
	.attr('class', function(d) { return 'mag_miniItem ' + d.class; })
	.attr('d', function(d) { return d.path; });
	mag_bar_items.exit().remove();
	
	m_mini.append('rect')
	.attr('pointer-events', 'painted')
	.attr('width', width)
	.attr('height', m_mini_height)
	.attr('visibility', 'hidden')
	.on('mousedown', displayTweet)
	.on('mousemove', function() { displayRelated(false, this);})
	.on('mouseout', cleanHovered);
	
	
	mag_brush = d3.svg.brush()
	.x(x)
	.extent([d3.time.monday(now),d3.time.saturday.ceil(now)])
	.on("brush", updateMagBrush);			// upudateBrush
	
	m_mini.append('g')
	.attr('class', 'x brush')
	.attr('bar','top')
	.call(mag_brush)
	.on('mousedown', displayTweet)
	.on('mousemove', function() { displayRelated(false, this);} )
	.on('mouseout', cleanHovered)
	.on('mousewheel', function() { zoomBrush(false, this);})
	.selectAll('rect')
		.attr('y', 1)
		.attr('height', m_mini_height - 1);
	
	m_mini.selectAll('rect.background').remove();
}

function showTweetInfoArea(){
	TweetInfoArea.append("text").attr({
		'class'	:	'feedback_msg'
	})
	.style("font-weight", "bold")
	.text("");
}

var brush_org_min, brush_org_max, milisec_per_pix;

function moveBrush () {
	var origin = d3.mouse(this)
	  , point = x.invert(origin[0])
//	  , halfExtent = (brush.extent()[1].getTime() - brush.extent()[0].getTime()) / 2
	  , halfExtent = 6 * 3600 * 1000		// 6 hours
	  , start = new Date(point.getTime() - halfExtent)
	  , end = new Date(point.getTime() + halfExtent);

	if(no_brush == false){
		deleteWindow();
	}
	
	if(no_brush == true){
		if(start < new Date(timebar_item_data.items[0].start)){
			start = new Date(timebar_item_data.items[0].start - 1);
		}

		if(end > new Date(timebar_item_data.items[timebar_item_data.items.length-1].end)){
			end = new Date(timebar_item_data.items[timebar_item_data.items.length-1].end + 1);
		}

		brush.extent([start,end]);
		brush_org_min = start, brush_org_max = end;
//		TweetInfoArea.select('.debug').text('mouse up at rect! - ' + start + ', ' + end);
		updateBrush();
		update_mag_timebar(false);
		no_brush = false;
		
		var p1 = origin[0], p2 = origin[0] + 1;
		milisec_per_pix = x.invert(p2).getTime() - x.invert(p1).getTime(); // 1087321
//		milisec_per_pix = 1100000;
		
		d3.selectAll(".m_mini").style('visibility', 'visible');
		d3.selectAll(".m_axis").style('visibility', 'visible');
		d3.selectAll(".mag_miniItem").style('visibility', 'visible');
		
		d3.selectAll(".x.brush").style('visibility', 'visible');
		d3.select("#pointer_control").style('visibility', 'visible');
		d3.select("#delete_window").style('visibility', 'visible');
		
		if(sankey_mode_passive == true){
			d3.selectAll(".p_over").style('visibility', 'visible');
		} else {
			d3.selectAll(".a_over").style('visibility', 'visible');
		}
	} 
}

function movePointer(e){
	e = e || window.event;

    if (e.keyCode == '37') {
    	if(mag_x != null) movePointerToLeft();
    }
    else if (e.keyCode == '39') {
    	if(mag_x != null) movePointerToRight();
    }
}

function movePointerToLeft(){
/*	if(highlight_mode != "none"){
		d3.select('.feedback_msg').text('disable the highlight mode to move the cursor');
		return;
	}
    */
//	var minExtent = d3.time.second(mag_brush.extent()[0]);
//	var minExtent = d3.time.second(mag_x.domain()[0]);
	
	var minExtent = d3.time.second(x.domain()[0]);
	
	if(pointer_index > 0){
		if(TweetInfo[pointer_index-1].Date * 1000 >= minExtent.getTime()){
			updateCursor(--pointer_index);	
			updateNetworkInfo(pointer_index, false);
			updateMagBarItems();
			focusNode();
		}
	}

	return;
}

function movePointerToRight(){
/*	if(highlight_mode != "none"){
		d3.select('.feedback_msg').text('disable the highlight mode to move the cursor');
		return;
	}
*/
//	var maxExtent = d3.time.second(mag_brush.extent()[1]);
//	var maxExtent = d3.time.second(mag_x.domain()[1]);
	
	var maxExtent = d3.time.second(x.domain()[1]);
	
	if(pointer_index < TweetInfo.length-1){
		if(TweetInfo[pointer_index+1].Date * 1000 <= maxExtent.getTime()){
			updateCursor(++pointer_index);
			updateNetworkInfo(pointer_index, false);
			updateMagBarItems();
			focusNode();
		}
	}
	
	return;
}

function deleteWindow(){
	no_brush = true;
	
	d3.selectAll(".m_mini").style('visibility', 'hidden');
	d3.selectAll(".m_axis").style('visibility', 'hidden');
	d3.selectAll(".mag_miniItem").style('visibility', 'hidden');

	d3.selectAll(".x.brush").style('visibility', 'hidden');
	d3.select("#delete_window").style('visibility', 'hidden');
	d3.selectAll(".p_over").style('visibility', 'hidden');
	d3.selectAll(".a_over").style('visibility', 'hidden');

	brush.extent([x.domain()[0],x.domain()[0]]);
	updateBrush();
	update_mag_timebar(false);
//	updateCursor(-1);

}

var prev_tweet_displayed = -1;

function checkAuthorMisMatch(Tweet, Author){
	var area_to_search = Tweet.substring(Tweet.lastIndexOf("https://twitter.com/"));
	
	if(area_to_search.toLowerCase().lastIndexOf(Author) == -1)
		return true;
	else
		return false;
}

function updateTweetInfo(selectedIndex){
//	TweetInfoArea.select('text.TweetInfo').text(TweetInfo[selectedIndex].Tweet);

	if(selectedIndex == -1) {
		d3.select('#TweetInfo').html("<p></p>");
		d3.select('#AuthorInfo p').html('ID: N/A <br> Followers: N/A <br> Followee: N/A <br>Rumor Tweet: N/A <br>Correction Tweet: N/A <br> Rumor Exposed: N/A <br> Correction Exposed: N/A');
		d3.select('#TimeInfo p').text("");
		return;
	}
	
	if(prev_tweet_displayed != selectedIndex){
		if(TweetInfo[selectedIndex].Tweet.length > 0){
			d3.select('#TweetInfo').html(TweetInfo[selectedIndex].Tweet);
			twttr.widgets.load();
			if(checkAuthorMisMatch(TweetInfo[selectedIndex].Tweet, TweetInfo[selectedIndex].Author)){
				d3.select('#AuthorInfo th').text('Retweeted by');
				d3.select('#TimeInfo th').text('Retweet Time');
			} else {
				d3.select('#AuthorInfo th').text('Author');
				d3.select('#TimeInfo th').text('Time of Tweet');
			}
		} else {
			d3.select('#TweetInfo').html("<p>Tweet Deleted</p>");
		}
		
		prev_tweet_displayed = selectedIndex;
	}
	var d = new Date(TweetInfo[selectedIndex].Date * 1000);
	d3.select('#TimeInfo p').text(d.toString().substring(0, d.toString().length-14));
	
	if(AuthorInfo[selectedIndex].RumorTweet != -1){
		var radio_button_html = "", common_part = '<input type="radio" name="hl" onclick="changeHighlight(\'';
		for(var options = 0; options < 4; options++){
			radio_button_html += common_part;
			if(options == 0) {
				radio_button_html += 'author\')" ';
				radio_button_html += (highlight_mode == 'author' ? 'checked>Author' : '>Author'); 
			}
			else if(options == 1) {
				radio_button_html += 'follower\')" ';
				radio_button_html += (highlight_mode == 'follower' ? 'checked>Follower' : '>Follower') + '<br>'; 
			}
			else if(options == 2) {
				radio_button_html += 'followee\')" ';
				radio_button_html += (highlight_mode == 'followee' ? 'checked>Followee' : '>Followee'); 
			}
			else if(options == 3) {
				radio_button_html += 'none\')" ';
				radio_button_html += (highlight_mode == 'none' ? 'checked>None' : '>None');
			}
		}
		d3.select('#AuthorInfo p').html('ID:' + TweetInfo[selectedIndex].Author + '<br> Followers: ' + 
			AuthorInfo[selectedIndex].FollowerCount + '<br>Followee: ' + AuthorInfo[selectedIndex].FolloweeCount + '<br>Rumor Tweet: ' + 
			AuthorInfo[selectedIndex].RumorTweet + ' times<br>Correction Tweet: ' + AuthorInfo[selectedIndex].DebunkTweet +
			' times<br> Rumor Exposed: ' + AuthorInfo[selectedIndex].RumorExp + ' times<br>Correction Exposed: ' + 
			AuthorInfo[selectedIndex].DebunkExp + ' times<br><br> Highlight Options <br><form action="">' + radio_button_html + '</form>');
//		d3.select("#highlight_choice").style('visibility', 'visible');
	} else {
		d3.select('#AuthorInfo p').html('ID: N/A <br> Followers: N/A <br> Followee: N/A <br>Rumor Tweet: N/A <br>Correction Tweet: N/A <br> Rumor Exposed: N/A <br> Correction Exposed: N/A');
//		d3.select("#highlight_choice").style('visibility', 'hidden');
	}
}

function updateCursor(selectedIndex){
	if(selectedIndex != -1){
		if(CursorStack.length > 0 && CursorStack[CursorStack.length-1] != selectedIndex){
            if(CursorStack.length == 1){
                document.getElementById("restoreButton").disabled = false; 
            }
            CursorStack.push(selectedIndex);
        }
        var xMonthAxis = d3.svg.axis()
			.scale(x)
			.orient('top')
			.tickValues([timebar_item_data.items[selectedIndex].start])
			.tickFormat('cursor')
			.tickSize(15, 0, 0);
		
		mini.transition().duration(0).select('.axis.month')
			.call(xMonthAxis).selectAll('text').attr('dx', 22).attr('dy', 12);
		
		if(no_brush == false){
			var m_xMonthAxis = d3.svg.axis()
			.scale(mag_x)
			.orient('top')
			.tickValues([timebar_item_data.items[selectedIndex].start])
			.tickFormat('cursor')
			.tickSize(15, 0, 0);
		
			m_mini.transition().duration(0).select('.m_axis.month')
				.call(m_xMonthAxis).attr('visibility', 'visible').selectAll('text').attr('dx', 22).attr('dy', 12);
		}
		updateTweetInfo(selectedIndex);
	} else {
		var xMonthAxis = d3.svg.axis()
		.scale(x)
		.orient('top')
		.tickValues([timebar_item_data.items[0].start])
		.tickFormat('cursor')
		.tickSize(15, 0, 0);
	
		mini.transition().duration(0).select('.axis.month')
			.call(xMonthAxis).selectAll('text').attr('dx', 22).attr('dy', 12);
	
		updateTweetInfo(0);
	}
}

var longItems = [], prev_selectedIndex = -1;

function updateNetworkInfo(selectedIndex, enforceupdate){
	if(enforceupdate == false && selectedIndex == prev_selectedIndex){
		return;
	}

	while(longItems.length > 0){
		var idx_of_tid = longItems.shift();
		var length = timebar_item_data.items[idx_of_tid].class.length;
		timebar_item_data.items[idx_of_tid].class = timebar_item_data.items[idx_of_tid].class.substring(0, length-9);
	}
	
//	var CurrentItemIdx = StateIndex[TweetID_Date[TweetInfo[selectedIndex].TweetID]];

	var data_string = null;
	if(highlight_mode == 'author'){
		data_string = TweetsOfNetwork[selectedIndex].TweetsOfAuthor;
	} else if(highlight_mode == 'follower'){
		data_string = TweetsOfNetwork[selectedIndex].TweetsOfFollowers;
	} else if(highlight_mode == 'followee'){
		data_string = TweetsOfNetwork[selectedIndex].TweetsOfFollowees;
	}
	
//    d3.select('.feedback_msg').text(selectedIndex);
	if(data_string != null && data_string.length > 1){
		highlight_tweets = data_string.split(',');
	} else {
		highlight_tweets = [];
	} 
	
	if(highlight_mode == 'follower' || highlight_mode == 'followee'){
		highlight_tweets.push(TweetInfo[selectedIndex].TweetID);		
	}
	
	if(highlight_tweets.length >= 1){
		for(var it = 0; it < highlight_tweets.length; it++){
			tid = highlight_tweets[it];
	//		alert(tid + ', ' + TweetID_Date[tid]);
			var idx_of_tid = StateIndex[TweetID_Date[tid]];
//			alert(idx_of_tid + ', type: ' + TweetInfo[idx_of_tid].Type + ', but: ' + timebar_item_data.items[idx_of_tid].class);
			timebar_item_data.items[idx_of_tid].class = timebar_item_data.items[idx_of_tid].class + ' longItem';
			longItems.push(idx_of_tid);
		}
	}
	
	updateBarItems();
	
	prev_selectedIndex = selectedIndex;
}

function updateMagBrush(){
	var minExtent = d3.time.second(mag_brush.extent()[0])
	  , maxExtent = d3.time.second(mag_brush.extent()[1]);
	
	m_mini.select('.brush').call(mag_brush.extent([minExtent, maxExtent]));
	mini.select('.brush').call(brush.extent([minExtent, maxExtent]));

    updatingMagBrush=true;
	updateBrush();
	updatingMagBrush=false;
//	var items = timebar_item_data.items.filter(function (val) { return ((val.start >= minExtet65nt) && (val.end <= maxExtent));});
	
	updateMagBarItems();

	/*
	var left_index = search_left_index(minExtent.getTime() / 1000);
	var right_index = search_right_index(maxExtent.getTime() / 1000);
	
	if(left_index <= right_index){
		updateCursor(left_index);		
	} else {
		updateCursor(-1);
	}

	pointer_index = left_index;
	*/
}

var dragging = false, brush_updating = false;

function brushdown(){
	dragging = false;
	brush_updating = true;
	brush_org_min = d3.time.second(brush.extent()[0]);
	brush_org_max = d3.time.second(brush.extent()[1]);
//	TweetInfoArea.select('.debug').text('brush down! - ' + brush_org_min.getTime().toString() + '('+ left_delta.toString() + '), ' + brush_org_max.getTime().toString() + '( ' + right_delta.toString() + ')');
}

var updatingMagBrush = false;
function updateBrush(){
	var minExtent = d3.time.second(brush.extent()[0])
	  , maxExtent = d3.time.second(brush.extent()[1]);

    if(updatingMagBrush != true){
	// prevent brush bumping bugs - d3 brush problem...
        if(Math.abs(minExtent.getTime() - brush_org_min.getTime()) <=  milisec_per_pix){
            minExtent = brush_org_min;
        }
        if(Math.abs(maxExtent.getTime() - brush_org_max.getTime()) <=  milisec_per_pix){
            maxExtent = brush_org_max;
        }
        if(Math.abs(maxExtent.getTime() - brush_org_min.getTime()) <=  milisec_per_pix){
            maxExtent = brush_org_min;
        }
        if(Math.abs(minExtent.getTime() - brush_org_max.getTime()) <=  milisec_per_pix){
            minExtent = brush_org_max;
        }
        
        if(dragging == false && minExtent != brush_org_min && minExtent != brush_org_max && maxExtent != brush_org_min && maxExtent != brush_org_max){
            dragging = true;
    //		TweetInfoArea.select('.debug').text('dragging');
        }

        if(minExtent.getTime() >= (brush_org_max.getTime() - milisec_per_pix) && 
                dragging == false){
            maxExtent = brush_org_max, minExtent = new Date(brush_org_max.getTime() - milisec_per_pix*2);
        }
        if(maxExtent.getTime() <= brush_org_min.getTime() && 
                dragging == false){
            maxExtent = new Date(brush_org_min.getTime() + milisec_per_pix*2), minExtent = brush_org_min;
        }
    }

	mini.select('.brush').call(brush.extent([minExtent, maxExtent]));
	var left_index = search_left_index(minExtent.getTime() / 1000);
	var right_index = search_right_index(maxExtent.getTime() / 1000);
	d3.select('.feedback_msg').text('brushing - ' + left_index+ ', ' + right_index);
	
	if(left_index <= right_index){
		initNT(TweetInfo[left_index].Date, TweetInfo[right_index].Date);
//		updateNetworkInfo(left_index, false);
	} else {
		initNT(-1, -1);
	}

//	TweetInfoArea.select('.debug').text('mag update complete - ' + minExtent.getTime().toString() + ', ' + maxExtent.getTime().toString());
}

var global_id_clock = 0;

function getPaths(items, x_scale, y_scale) {
	var paths = {}, d, offset = .5 * y_scale(1) + 0.5, result = [];
	for (var i = 0; i < items.length; i++) {
		d = items[i];
		if (!paths[d.class]) paths[d.class] = '';
		if(endsWith(d.class, 'm') || endsWith(d.class, 'ed')){
			paths[d.class] += ['M',x_scale(d.start),(y_scale(d.lane) + offset),'H',x_scale(d.start)+3].join(' ');
		} else {
			paths[d.class] += ['M',x_scale(d.start),(y_scale(d.lane) + offset),'H',x_scale(d.start)+1].join(' ');			
		}
	}

	for (var className in paths) {
		result.push({class: className, path: paths[className], timestamp: global_id_clock});
	}
	global_id_clock++;
	return result;
}

function endsWith(str, suffix) {
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
}

var highlight_mode = 'none';
var no_brush = true;

function changeHighlight(mode){
	highlight_mode = mode;
	
	if(mode == 'none'){
		d3.select('.feedback_msg').text('');
	}
	if(pointer_index !=  -1) {
		updateNetworkInfo(pointer_index, true);
		if(no_brush == false)
			updateMagBarItems();
	}
}

function restoreCursor(){
    if(CursorStack.length > 1){
        CursorStack.pop();
        pointer_index = CursorStack[CursorStack.length-1];
        updateCursor(pointer_index);
        updateNetworkInfo(pointer_index, false);
        updateMagBarItems();
    } 
    
    if(CursorStack.length == 1){
        //disable button
        document.getElementById("restoreButton").disabled = true; 
    }
}
