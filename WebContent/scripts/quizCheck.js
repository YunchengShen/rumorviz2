function checkQuiz1_1(choice){
	if(choice == 'd'){
		d3.select('#explanation').text('Correct!');
        d3.select('button#proceed').style('visibility', 'visible');
	} else {
		var radiobtn = document.getElementById("PMode");
		radiobtn.checked = true;
		changeSankeyMode('Exposed');
		d3.select('#explanation').text('Hint: People who get exposed to the correction after the rumor, and those who get exposed to the rumor after the correction move to the "Both Exposure"');
	}
}

function checkQuiz1_2(choice){
	if(sankey_mode_passive == true){
		d3.select('#explanation').text('You haven\'t switched the diagram to active user mode. Use the radio button at the top right area of the diagram to switch the mode');
		return;
	}
	
	if(choice == 'd'){
		d3.select('#explanation').text('Correct!');
	} else {
		d3.select('#explanation').text('Hint: the grey flow from "Rumor Exposure 2" to "Tweet Rumor 1" represents the population');
		return;
	}
}

function checkQuiz2(){
	if(no_brush == true){
		d3.select('#explanation').text('You haven\'t created a time window yet!');
		return;
	} else {
		var minExtent = d3.time.second(brush.extent()[0])
		  , maxExtent = d3.time.second(brush.extent()[1]);
		
		var diff = maxExtent.getTime() - minExtent.getTime(), two_days = 1000 * 3600 * 12, two_half_days = 1000 * 3600 * 15;
		
		if(diff < two_days){
			d3.select('#explanation').text('Make the area a little bit larger');
			return;
		} 
		
		if(diff > two_half_days){
			d3.select('#explanation').text('Make the area a little bit smaller');
			return;
		}
		d3.select('#explanation').text('Great!');
        d3.select('button#proceed').style('visibility', 'visible');
		return;
	}
}

function checkQuiz3(choice){
	if(pointer_index == 2529){
		if(choice == 'a'){
			d3.select('#explanation').text('Correct!');			
            d3.select('button#proceed').style('visibility', 'visible');
		} else {
			d3.select('#explanation').text('You found the right tweet but haven\'t got the answer correctly. Find the number from the box on the right hand side!');			
		}
	} else {
		d3.select('#explanation').text('The cursor is not pointing to the right tweet. You can move the cursor by clicking an item at the magnified timeline view or using the arrow keys');
	}
}

function checkQuiz4(choice){
	if(pointer_index != 2554){
		d3.select('#explanation').text('The cursor is not pointing to the right tweet. You can move the cursor by clicking an item at the magnified timeline view or using the arrow keys');
		return;
	}

	if(highlight_mode == 'followee'){
		if(choice == 'a'){
			d3.select('#explanation').text('Correct!');			
            d3.select('button#proceed').style('visibility', 'visible');
		} else {
			d3.select('#explanation').text('Hover over the highlighted items. They represent the tweets made by the followers.');			
		}
	} else {
		d3.select('#explanation').text('You are not highlighting the tweets of the people who are followed by this person! Use the radio button in the Author box on the right hand side');
	}
}
