var EdgeToTweetIndex = new Array();			// Index of Tweets related to a particular Edge. 
var ETTI_Ubound = new Array();				// keep the allocated array size

function createXMLHttpRequest(){
	  // See http://en.wikipedia.org/wiki/XMLHttpRequest
	  // Provide the XMLHttpRequest class for IE 5.x-6.x:
	  if( typeof XMLHttpRequest == "undefined" ) XMLHttpRequest = function() {
	    try { return new ActiveXObject("Msxml2.XMLHTTP.6.0"); } catch(e) {}
	    try { return new ActiveXObject("Msxml2.XMLHTTP.3.0"); } catch(e) {}
	    try { return new ActiveXObject("Msxml2.XMLHTTP"); } catch(e) {}
	    try { return new ActiveXObject("Microsoft.XMLHTTP"); } catch(e) {}
	    throw new Error( "This browser does not support XMLHttpRequest." );
	  };
	  return new XMLHttpRequest();
}

var AJAX = createXMLHttpRequest();
var LinkAJAX = createXMLHttpRequest();

function dataRequest(rumorID){
//	restart();
	AJAX.onreadystatechange = cacheData;
	AJAX.open("GET", "fetch_data.jsp?RID=" + rumorID);
	AJAX.send("");
}

function dataRequest_NT(rumorID){	
	AJAX.onreadystatechange = cacheDataNT;
	AJAX.open("GET", "fetch_data_nt.jsp?RID=" + rumorID);
	AJAX.send("");
	
	LinkAJAX.onreadystatechange = cacheLink;
	LinkAJAX.open("GET", "fetch_link.jsp?RID=" + rumorID);
	LinkAJAX.send("");
}

var PassiveStateUpdates = [], ActiveStateUpdates = []; 
var TweetsOfNetwork = [], TweetInfo = [], AuthorInfo = [], LinkInfo = [], NodeInfo = [], AuthorOrder = [];
var StateIndex = new Array();
var TweetID_Date = new Array();

var cachingNTdone = false;

function cacheDataNT(){
	if(AJAX.readyState == 4 && AJAX.status == 200) {
		var json = eval('(' + AJAX.responseText +')');
		var OrderCounter = 0;
		for(var i = 0; i < json.length; i++){
			if(AuthorOrder[json[i].Author] == null){
				AuthorOrder[json[i].Author] = OrderCounter++;
				NodeInfo.push(
					{
						name	: 	json[i].Author,
						size	:	json[i].Author_Follower,
						display	: 	0,
						type	:	json[i].Type,							// 1: rumor, 2: correction
						first_tweet_idx	: i,
						focus	:	false
					});
			} else {
				if(NodeInfo[AuthorOrder[json[i].Author]].type != json[i].Type)
					NodeInfo[AuthorOrder[json[i].Author]].type = 3;		// both rumor and correction
			}
			
			TweetsOfNetwork.push(
				{
					TweetsOfFollowers:	json[i].TweetOfFollowers,
					TweetsOfFollowees:	json[i].TweetOfFollowees,
					TweetsOfAuthor:		json[i].TweetOfAuthors
				});
				
			TweetInfo.push(
				{
					Type			:	json[i].Type,
					Tweet			:	json[i].Tweet,
					Author			:	json[i].Author,
					TweetID			:	json[i].TweetID,
					Date			:	json[i].Date
				});
			
			AuthorInfo.push(
				{
					FollowerCount	: json[i].Author_Follower,
					FolloweeCount	: json[i].Author_Followee,
					RumorExp		: json[i].Author_RumorExp,
					DebunkExp		: json[i].Author_DebunkExp,
					RumorTweet		: json[i].Author_RumorTweet,
					DebunkTweet		: json[i].Author_DebunkTweet
				});
			
			StateIndex[json[i].Date.toString()] = i;
			TweetID_Date[json[i].TweetID] = json[i].Date.toString();
		}
//		alert('Node count - ' + NodeInfo.length);
		if(cachingNTdone == true){
			initNT(-1, -1);
			dummy();
		} else {
			cachingNTdone = true;			
		}

	} else if (AJAX.readyState == 4 && AJAX.status != 200) {
	//	alert('Something went wrong...');
	}
}

function cacheLink(){
	if(LinkAJAX.readyState == 4 && LinkAJAX.status == 200) {
		var json = eval('(' + LinkAJAX.responseText +')');
		for(var i = 0; i < json.length; i++){
			LinkInfo.push(
				{
					source	:	json[i].source,
					target	: 	json[i].target
				});
		}
//		alert('Link count - ' + LinkInfo.length);
		if(cachingNTdone == true){
			initNT(-1, -1);
			dummy();
		} else {
			cachingNTdone = true;
		}
		
	} else if (LinkAJAX.readyState == 4 && LinkAJAX.status != 200) {
//		alert('Something went wrong...');
	}
}

var prev_InitIndex = -1, prev_EndIndex = -1;

function initNT(first_timestamp, end_timestamp){
	var init_index = 0, end_index = TweetInfo.length-1;
	
	if(first_timestamp != -1 || window_updated == true){
		if(StateIndex[first_timestamp.toString()] != null)
			init_index = StateIndex[first_timestamp.toString()];
	} 
	
	if(end_timestamp != -1 || window_updated == true){
		if(StateIndex[end_timestamp.toString()] != null)
			end_index = StateIndex[end_timestamp.toString()];
	}
	
	if(end_index == 0) 
		end_index = TweetInfo.length-1;
	if(prev_InitIndex == init_index && prev_EndIndex == end_index)
		return;
	
	for(var i = 0; i < TweetInfo.length; i++){
		var order = AuthorOrder[TweetInfo[i].Author];
		if(i >= init_index && i <= end_index){
			NodeInfo[order].display = 1;			
		} else {
			NodeInfo[order].display = 0;
		}
	}
	if(first_timestamp != -1 && end_timestamp != -1){
		window_updated = true;
	}

	prev_InitIndex = init_index, prev_EndIndex = end_index;
	restart();
}

function cacheData(){
	if(AJAX.readyState == 4 && AJAX.status == 200) {
		var json = eval('(' + AJAX.responseText +')');
		for(var i = 0; i < json.length; i++){			
			var PTransitionInstance = new Array(), ATransitionInstance = new Array();
			for(var j = 0; j < TransitionNames.length; j++){
				PTransitionInstance.push(json[i]['P' + TransitionNames[j]]);
				ATransitionInstance.push(json[i]['A' + TransitionNames[j]]);
			}
			PassiveStateUpdates.push(PTransitionInstance);
			ActiveStateUpdates.push(ATransitionInstance);
			
			TweetsOfNetwork.push(
				{
					TweetsOfFollowers:	json[i].TweetOfFollowers,
					TweetsOfFollowees:	json[i].TweetOfFollowees,
					TweetsOfAuthor:		json[i].TweetOfAuthors
				});
			
			TweetInfo.push(
				{
					Type			:	json[i].Type,
					Tweet			:	json[i].Tweet,
					Author			:	json[i].Author,
					TweetID			:	json[i].TweetID,
					Date			:	json[i].Date
				});
			
			AuthorInfo.push(
				{
					FollowerCount	: json[i].Author_Follower,
					FolloweeCount	: json[i].Author_Followee,
					RumorExp		: json[i].Author_RumorExp,
					DebunkExp		: json[i].Author_DebunkExp,
					RumorTweet		: json[i].Author_RumorTweet,
					DebunkTweet		: json[i].Author_DebunkTweet
				});
			
			StateIndex[json[i].Date.toString()] = i;
			TweetID_Date[json[i].TweetID] = json[i].Date.toString();
		}
		for(var i = 0; i < TransitionNames.length; i++){
			EdgeToTweetIndex[TransitionNames[i]] = new Array();
		}
		initSD(-1, -1);
		dummy();
	} else if (AJAX.readyState == 4 && AJAX.status != 200) {
//		alert('Something went wrong...');
	}
}

function initSD(first_timestamp, end_timestamp){
	var init_index = 0, end_index = PassiveStateUpdates.length-1;
	
	if(first_timestamp != -1 || window_updated == true){
		init_index = StateIndex[first_timestamp.toString()];
	} 
	
	if(end_timestamp != -1 || window_updated == true){
		end_index = StateIndex[end_timestamp.toString()];			
	}

	for(var i = 0; i < TransitionNames.length; i++){
		Pedge_window[i] = 0;
		Aedge_window[i] = 0;
	}

	for(var i = 0; i < TransitionNames.length; i++){
		ETTI_Ubound[TransitionNames[i]] = 0;
	}
	var reg_patt = /Twt[1-3]$/;
	
	while(init_index <= end_index){
		var Edge_Name = "";
		for(var i = 0; i < TransitionNames.length; i++){
			Pedge_window[i] += PassiveStateUpdates[init_index][i];
			Aedge_window[i] += ActiveStateUpdates[init_index][i];
			
//			if(endsWith(TransitionNames[i], "Twt") == true && ActiveStateUpdates[init_index][i] == 1)
			if(reg_patt.test(TransitionNames[i]) == true && ActiveStateUpdates[init_index][i] == 1)
				Edge_Name = TransitionNames[i];
		}

//		TweetInfoArea.select('.debug').text('Edge ID - ' + Edge_id);
		if(Edge_Name.length > 1){
			if(EdgeToTweetIndex[Edge_Name].length <= ETTI_Ubound[Edge_Name]){
				EdgeToTweetIndex[Edge_Name].push(init_index);
			} else {
				EdgeToTweetIndex[Edge_Name][ETTI_Ubound[Edge_Name]] = init_index;
			}
			ETTI_Ubound[Edge_Name]++;
		}

		init_index++;
	}
	
/*	for(var i = 0; i < TransitionNames.length; i++){
		alert(TransitionNames[i] + ": " + PEdgeWidth[i] + ", " + AEdgeWidth[i]);		
	}
*/
	if(first_timestamp != -1 && end_timestamp != -1){
		window_updated = true;
	}

	for(var i = 0; i < Pedge_window.length; i++){
		if(Pedge_window[i] == 0){
			Pedge_window[i] = 0.1;
		}
		if(Aedge_window[i] == 0){
			Aedge_window[i] = 0.1;
		}
	}
	restart();
//	TweetInfoArea.select('.debug').text('initSD log - ' + first_timestamp + ', ' + end_timestamp);
}

function search_left_index(leftTimestamp){
	var left = 0, right = TweetInfo.length-1;
	var mid = -1;
	
	while(left <= right){
		mid = Math.floor((left + right) / 2);
		if(left == right) break;
//		alert('1. ' + left + ', ' + mid + ', ' + right + ', ' + TweetInfo[mid].Date + ', '  + leftTimestamp);
		if(TweetInfo[mid].Date > leftTimestamp){
			right = mid-1;
		} else if (TweetInfo[mid].Date < leftTimestamp){
			left = mid+1;
		} else {
			break;
		}
	}
//	alert(mid + ', ' + leftTimestamp + ', ' + TweetInfo[mid].Date);
	if(leftTimestamp > TweetInfo[mid].Date){
		return mid+1;
	} else {
		return (mid);		
	}
}

function search_right_index(rightTimestamp){
	var left = 0, right = TweetInfo.length-1;
	var mid = -1;
	while(left <= right){
		mid = Math.floor((left + right) / 2);
		if(left == right) break;
//		alert('2. ' + left + ', ' + mid + ', ' + right);
		if(TweetInfo[mid].Date > rightTimestamp){
			right = mid-1;
		} else if (TweetInfo[mid].Date < rightTimestamp){
			left = mid+1;
		} else {
			break;
		}
	}

	if(rightTimestamp < TweetInfo[mid].Date){
		return mid-1;
	} else {
		return mid;
	}
}
