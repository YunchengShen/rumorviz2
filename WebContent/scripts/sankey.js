d3.sankey = function() {
  var sankey = {},
      nodeWidth = 24,
      nodePadding = 8,
      size = [1, 1],
      nodes = [],
      links = [];

  sankey.nodeWidth = function(_) {
    if (!arguments.length) return nodeWidth;
    nodeWidth = +_;
    return sankey;
  };

  sankey.nodePadding = function(_) {
    if (!arguments.length) return nodePadding;
    nodePadding = +_;
    return sankey;
  };

  sankey.nodes = function(_) {
    if (!arguments.length) return nodes;
    nodes = _;
    return sankey;
  };

  sankey.links = function(_) {
    if (!arguments.length) return links;
    links = _;
    return sankey;
  };

  sankey.size = function(_) {
    if (!arguments.length) return size;
    size = _;
    return sankey;
  };

  sankey.layout = function(iterations) {
    computeNodeLinks();
    computeNodeValues();
    computeNodeBreadths();
    computeNodeDepths(iterations);
    computeLinkDepths();
    return sankey;
  };

  sankey.relayout = function() {
    setLinkOrders();
    computeLinkDepths();
    return sankey;
  };

  sankey.shiftLinkOfNode = function (ReorderSource, node, link, LeftClick) {
      shiftLinkOfNode(ReorderSource, node, link, LeftClick);
  };

  sankey.link = function() {
    var curvature = .5;

    function link(d) {
      var x0 = d.source.x + d.source.dx,
          x1 = d.target.x,
          xi = d3.interpolateNumber(x0, x1),
          x2 = xi(curvature),
          x3 = xi(1 - curvature),
          y0 = d.source.y + d.sy + d.dy / 2,
          y1 = d.target.y + d.ty + d.dy / 2;
      return "M" + x0 + "," + y0
           + "C" + x2 + "," + y0
           + " " + x3 + "," + y1
           + " " + x1 + "," + y1;
    }

    link.curvature = function(_) {
      if (!arguments.length) return curvature;
      curvature = +_;
      return link;
    };

    return link;
  };

  // Populate the sourceLinks and targetLinks for each node.
  // Also, if the source and target are not objects, assume they are indices.
  function computeNodeLinks() {
    nodes.forEach(function(node) {
      node.sourceLinks = [];
      node.targetLinks = [];
    });
    links.forEach(function(link) {
      var source = link.source,
          target = link.target;
      if (typeof source === "number") source = link.source = nodes[link.source];
      if (typeof target === "number") target = link.target = nodes[link.target];
      source.sourceLinks.push(link);
      target.targetLinks.push(link);
    });
  }

  // Compute the value (size) of each node by summing the associated links.
  function computeNodeValues() {
    nodes.forEach(function(node) {
      node.value = Math.max(
        d3.sum(node.sourceLinks, value),
        d3.sum(node.targetLinks, value)
      );
    });
  }

  // Iteratively assign the breadth (x-position) for each node.
  // Nodes are assigned the maximum breadth of incoming neighbors plus one;
  // nodes with no incoming links are assigned breadth zero, while
  // nodes with no outgoing links are assigned the maximum breadth.
  function computeNodeBreadths() {
    var remainingNodes = nodes,
        nextNodes,
        x = 0;

    while (remainingNodes.length) {
      nextNodes = [];
      remainingNodes.forEach(function(node) {
        node.x = x;
        node.dx = nodeWidth;
        node.sourceLinks.forEach(function(link) {
          nextNodes.push(link.target);
        });
      });
      remainingNodes = nextNodes;
      ++x;
    }

    //
    moveSinksRight(x);
    scaleNodeBreadths((size[0] - nodeWidth) / (x - 1));
  }

  function moveSourcesRight() {
    nodes.forEach(function(node) {
      if (!node.targetLinks.length) {
        node.x = d3.min(node.sourceLinks, function(d) { return d.target.x; }) - 1;
      }
    });
  }

  function moveSinksRight(x) {
    nodes.forEach(function(node) {
      if (!node.sourceLinks.length) {
        node.x = x - 1;
      }
    });
  }

  function scaleNodeBreadths(kx) {
    nodes.forEach(function(node) {
      node.x *= kx;
    });
  }

  function computeNodeDepths(iterations) {
    var nodesByBreadth = d3.nest()
        .key(function(d) { return d.x; })
        .sortKeys(d3.ascending)
        .entries(nodes)
        .map(function(d) { return d.values; });

    //
    initializeNodeDepth();
    resolveCollisions();
    for (var alpha = 1; iterations > 0; --iterations) {
      relaxRightToLeft(alpha *= .99);
      resolveCollisions();
      relaxLeftToRight(alpha);
      resolveCollisions();
    }

    function initializeNodeDepth() {
      var ky = d3.min(nodesByBreadth, function(nodes) {
           return (size[1] - (nodes.length - 1) * nodePadding) / d3.sum(nodes, value);
      });

      if(nodes.length == 15) ky *= ((size[1]-80)/size[1]);
      nodesByBreadth.forEach(function(nodes) {
        nodes.forEach(function(node, i) {
          node.y = i;
          node.dy = node.value * ky;
        });
      });

      links.forEach(function(link) {
        link.dy = link.value * ky;
      });
    }

    function relaxLeftToRight(alpha) {
      nodesByBreadth.forEach(function(nodes, breadth) {
        nodes.forEach(function(node) {
          if (node.targetLinks.length) {
            var y = d3.sum(node.targetLinks, weightedSource) / d3.sum(node.targetLinks, value);
            node.y += (y - center(node)) * alpha;
          }
        });
      });

      function weightedSource(link) {
        return center(link.source) * link.value;
      }
    }

    function relaxRightToLeft(alpha) {
      nodesByBreadth.slice().reverse().forEach(function(nodes) {
        nodes.forEach(function(node) {
          if (node.sourceLinks.length) {
            var y = d3.sum(node.sourceLinks, weightedTarget) / d3.sum(node.sourceLinks, value);
            node.y += (y - center(node)) * alpha;
          }
        });
      });

      function weightedTarget(link) {
        return center(link.target) * link.value;
      }
    }

    function resolveCollisions() {
      nodesByBreadth.forEach(function(nodes) {
        var node,
            dy,
            y0 = 0,
            n = nodes.length,
            i;

        // Push any overlapping nodes down.
        nodes.sort(ascendingDepth);
        for (i = 0; i < n; ++i) {
          node = nodes[i];
          dy = y0 - node.y;
          if (dy > 0) node.y += dy;
          y0 = node.y + node.dy + nodePadding;
        }

        // If the bottommost node goes outside the bounds, push it back up.
        dy = y0 - nodePadding - size[1];
        if (dy > 0) {
          y0 = node.y -= dy;

          // Push any overlapping nodes back up.
          for (i = n - 2; i >= 0; --i) {
            node = nodes[i];
            dy = node.y + node.dy + nodePadding - y0;
            if (dy > 0) node.y -= dy;
            y0 = node.y;
          }
        }
      });
    }

    function ascendingDepth(a, b) {
      return a.y - b.y;
    }
  }

  function computeLinkDepths() {
/*  
    nodes.forEach(function(node) {
      node.sourceLinks.sort(ascendingTargetDepth);
      node.targetLinks.sort(ascendingSourceDepth);
    });
*/
    nodes.forEach(function(node) {
      var sy = 0, ty = 0;
      node.sourceLinks.forEach(function(link) {
        link.sy = sy;
        sy += link.dy;
      });
      node.targetLinks.forEach(function(link) {
        link.ty = ty;
        ty += link.dy;
      });
    });

    function ascendingSourceDepth(a, b) {
    	return a.source.y - b.source.y;
//    	return a.dy - b.dy;
    }

    function ascendingTargetDepth(a, b) {
    	return a.target.y - b.target.y;
//    	return a.dy - b.dy;
    }
  }

  function setLinkOrders(){
      // sourceLinks - links that have the node as the source
      if(nodes.length == 15){
          for(var i = 0; i < nodes.length; i++){
             if(nodes[i].name == "Start"){
                 var OrderByName = ["Tweet Rumor 1", "Rumor Exposure 1", "Correction Exposure 1", "Tweet Correction 1"];
                 performOrdering(i, OrderByName, true);
             } else if(nodes[i].name == "Rumor Exposure 1"){
                 var OrderByName = ["Tweet Rumor 1", "Rumor Exposure 2", "Both Exposure", "Tweet Correction 1"];
                 performOrdering(i, OrderByName, true);
             } else if(nodes[i].name == "Rumor Exposure 2"){
                 var OrderByName = ["Tweet Rumor 1", "Rumor Exposure 3+", "Both Exposure", "Tweet Correction 1"];
                 performOrdering(i, OrderByName, true);
             } else if(nodes[i].name == "Rumor Exposure 3+"){
                 var OrderByName = ["Tweet Rumor 1", "Both Exposure", "Tweet Correction 1"];
                 performOrdering(i, OrderByName, true);
             } else if(nodes[i].name == "Correction Exposure 1"){
                 var OrderByName = ["Tweet Rumor 1", "Both Exposure", "Correction Exposure 2", "Tweet Correction 1"];
                 performOrdering(i, OrderByName, true);
             } else if(nodes[i].name == "Correction Exposure 2"){
                 var OrderByName = ["Tweet Rumor 1", "Both Exposure", "Correction Exposure 3+", "Tweet Correction 1"];
                 performOrdering(i, OrderByName, true);
             } else if(nodes[i].name == "Correction Exposure 3+"){
                 var OrderByName = ["Tweet Rumor 1", "Both Exposure", "Tweet Correction 1"];
                 performOrdering(i, OrderByName, true);
             } else if(nodes[i].name == "Both Exposure"){
                 var OrderByName = ["Rumor Exposure 3+", "Rumor Exposure 2", "Rumor Exposure 1", "Correction Exposure 1", "Correction Exposure 2", "Correction Exposure 3+"];
                 performOrdering(i, OrderByName, false);
                 OrderByName = ["Tweet Rumor 1", "Tweet Correction 1"];
                 performOrdering(i, OrderByName, true);
             } else if(nodes[i].name == "Tweet Rumor 1"){
                 var OrderByName = ["Start", "Rumor Exposure 1", "Rumor Exposure 2", "Rumor Exposure 3+", "Correction Exposure 1", "Correction Exposure 2", "Correction Exposure 3+", "Both Exposure"];
                 performOrdering(i, OrderByName, false);
                 OrderByName = ["Tweet Both", "Tweet Rumor 2"];
                 performOrdering(i, OrderByName, true);
             } else if(nodes[i].name == "Tweet Rumor 2"){
                 OrderByName = ["Tweet Both", "Tweet Rumor 3+"];
                 performOrdering(i, OrderByName, true);
             } else if(nodes[i].name == "Tweet Correction 1"){
                 var OrderByName = ["Both Exposure", "Rumor Exposure 3+", "Rumor Exposure 2", "Rumor Exposure 1", "Correction Exposure 3+", "Correction Exposure 2", "Correction Exposure 1", "Start"];
                 performOrdering(i, OrderByName, false);
                 OrderByName = ["Tweet Correction 2", "Tweet Both"];
                 performOrdering(i, OrderByName, true);
             } else if(nodes[i].name == "Tweet Correction 2"){
                 OrderByName = ["Tweet Correction 3+", "Tweet Both"];
                 performOrdering(i, OrderByName, true);
             } 
          }
      } else {
          for(var i = 0; i < nodes.length; i++){
              if(nodes[i].name == "Start"){
                 var OrderByName = ["Rumor Exposure 1", "Correction Exposure 1"];
                 performOrdering(i, OrderByName, true);
                } else if(nodes[i].name == "Rumor Exposure 1"){
                 var OrderByName = ["Both Exposure", "Rumor Exposure 2"];
                 performOrdering(i, OrderByName, true);
                } else if(nodes[i].name == "Rumor Exposure 2"){
                 var OrderByName = ["Both Exposure", "Rumor Exposure 3+"];
                 performOrdering(i, OrderByName, true);
                } else if(nodes[i].name == "Correction Exposure 1"){
                 var OrderByName = ["Correction Exposure 2", "Both Exposure"];
                 performOrdering(i, OrderByName, true);
                } else if(nodes[i].name == "Correction Exposure 2"){
                 var OrderByName = ["Correction Exposure 3+", "Both Exposure"];
                 performOrdering(i, OrderByName, true);
                }else if(nodes[i].name == "Both Exposure"){
                 var OrderByName = ["Rumor Exposure 1", "Rumor Exposure 2", "Rumor Exposure 3+", "Correction Exposure 3+", "Correction Exposure 2", "Correction Exposure 1"];
                 performOrdering(i, OrderByName, false);
                }
          }

      }
  }

  function performOrdering(NodeIndex, Order, OrderSourceLinks){
      var OriginalArray = (OrderSourceLinks == true) ? nodes[NodeIndex].sourceLinks : nodes[NodeIndex].targetLinks;
      var NewArray = new Array(OriginalArray.length);
      for(var i = 0; i < OriginalArray.length; i++){
          for(var j = 0; j < Order.length; j++){
              if(OrderSourceLinks == true){
                  if(OriginalArray[i].target.name == Order[j]){
                      NewArray[j] = OriginalArray[i];
                      break;
                  }
              } else if(OrderSourceLinks == false){
                if(OriginalArray[i].source.name == Order[j]){
                  NewArray[j] = OriginalArray[i];
                  break;
                }
              }
          }
      }
      if(OrderSourceLinks == true)
          nodes[NodeIndex].sourceLinks = NewArray;
      else
          nodes[NodeIndex].targetLinks = NewArray;
  }

  function shiftLinkOfNode(ReorderSource, node, link, LeftClick){
	  var LinkArray = (ReorderSource == false) ? node.sourceLinks : node.targetLinks;
	  var LinkPosition = -1;

	  for(var i = 0; i < LinkArray.length ; i++){
		  if(LinkArray[i].sy == link.sy && LinkArray[i].ty == link.ty){
			  LinkPosition = i;
			  break;
		  }
	  }
      if(LeftClick == true && LinkPosition > 0){
         var temp = LinkArray[LinkPosition];
         LinkArray[LinkPosition] = LinkArray[LinkPosition-1];
         LinkArray[LinkPosition-1] = temp;
      } else if(LeftClick == false && LinkPosition < LinkArray.length-1){
         var temp = LinkArray[LinkPosition];
         LinkArray[LinkPosition] = LinkArray[LinkPosition+1];
         LinkArray[LinkPosition+1] = temp;
      }
	  
//	  alert('data structure reordering complete');
	  var sy = 0, ty = 0;
	  if(ReorderSource == false){
//		  node.sourceLinks = NewLinkArray;
		  node.sourceLinks.forEach(function(l) {
		        l.sy = sy;
		        sy += l.dy;
		  });
	  } else {
//		  node.targetLinks = NewLinkArray;
		  node.targetLinks.forEach(function(l) {
		        l.ty = ty;
		        ty += l.dy;
		  });
	  }
//	  alert('render reordering complete');
  }

  function center(node) {
    return node.y + node.dy / 2;
  }

  function value(link) {
    return link.value;
  }

  return sankey;
};
