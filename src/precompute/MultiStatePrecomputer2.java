package precompute;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import precompute.StateManager.Relation;
import precompute.StateManager.State;
import precompute.StateManager.TweetType;
import precompute.StateManager.UserType;

import db.DBManager;
import db.TimeStamp;

/**
 * This is a revamped version of MultiStatePrecomputer. It adds some additional states which account for different
 * numbers of retweeting behavior, and also streamlines the original class and tries to cut down on cookie
 * cutter code. 
 * @author Sam
 *
 */

public class MultiStatePrecomputer2
{
	private DBManager database;
	private HashMap<String,Long> idMap; //Maps Author names (or toString()-ed Author IDs) to Twitter IDs
	private HashMap<String,ArrayList<Long>> followerMap; //Maps Twitter names to sets of follower IDs
	
	private static final int rumor_type = 1; //Indicates that we are dealing with the AP explosion rumor
	private static final int population = 4868413; 
	private static final int start = -1; //start and end pertain to how many tweets we read into the tweet timeline
	private static final int end = -1;
	
/*	private static final int rumor_type = 0; //Indicates that we are dealing with the Russian Meteor rumor set. 
	private static final int population = 1451046; //Total number of twitter users associated with this rumor over all timespan
	private static final int start = 0; //start and end pertain to how many tweets we read into the tweet timeline
	private static final int end = 1361920704; */
	
	private ArrayList<TimeStamp> tweetTimeline;
	
	private StateManager manager; //Tracks the evolving state transition diagram
	
	public static void main(String[] args) throws Exception
	{
		System.out.println("Initializing MultiStatePrecomputer...");
		MultiStatePrecomputer2 sp = new MultiStatePrecomputer2();
		System.out.println("Precomputing states...");
		sp.precomputeStates();
		System.out.println("Done.");
	}
	
	public MultiStatePrecomputer2()
	{
		// TODO Auto-generated method stub
		System.out.println("Initializing DBManager...");
		database = new DBManager();
		System.out.println("Getting tweeter names");
		idMap = database.TweeterNameToID(rumor_type);
		System.out.println(idMap.size() + " name-ID mappings found.");
		System.out.println("Getting followers");
		followerMap = database.selectAllFollower(rumor_type);
		System.out.println(followerMap.size() + " followers found.");
		
	}

	private void precomputeStates() throws Exception
	{
		System.out.println("Retrieving tweet timeline from database...");
		tweetTimeline = database.selectTweetTimeline(start, end, rumor_type);
		System.out.println(tweetTimeline.size() + " tweets found.");
		
		System.out.println("Identifying all active users in tweet set");
		HashSet<Long> activeUsers = new HashSet<Long>();
		for (TimeStamp timeStamp : tweetTimeline)
		{
			if (getID(timeStamp) != null) 
				activeUsers.add(getID(timeStamp));		
		}
		System.out.println(activeUsers.size() + " active users found...");
		
		System.out.println("Precomputing state transitions...");
		
		manager = new StateManager(population, activeUsers.size()); //Initialize a new state manager with the total number of passive and active users
		
		//For each tweet, move people between states depending on the state they were in and how (if at all)
		//they are related to the current tweet (did they tweet it, do they follow its author)
		
		for (int i = 0; i < tweetTimeline.size(); i++)
		{
			manager.clearUpdates(); //Definitely don't forget to do this			
			TimeStamp tweet = tweetTimeline.get(i);
			TweetType tweetType = tweet.type == 1 ? TweetType.rumor:TweetType.debunk;
			
			//Write a snapshot of current state diagram to DB
			for (StateManager.UserType type : StateManager.UserType.values())
			{
				database.multiUpdateInitState(rumor_type, tweet.tweet_id, tweet.date, manager ,type);
			}
			
			//Manage how the tweet affects the movement of its author
			Long authorID = getID(tweet);
			if (authorID != null)
			{
				State source = manager.getCurrentState(authorID);
				State destination = StateManager.transitionMap.get(source).get(tweetType).get(Relation.twt);
				manager.moveUserToState(authorID, destination, UserType.active);
			}
					
			//Manage how the tweet affects movement of people exposed to it
			if (hasFollowers(tweet))
			{
				for (Long followerID :  getFollowers(tweet))
				{
					UserType followerType = activeUsers.contains(followerID) ? UserType.active : UserType.passive;
					State source = manager.getCurrentState(followerID);
					State destination = StateManager.transitionMap.get(source).get(tweetType).get(Relation.exp);
					
					manager.moveUserToState(followerID, destination, followerType);
				}
			}
			
			//Write a snapshot of the tweet's impact to DB
			for (StateManager.UserType type : StateManager.UserType.values())
			{
				database.multiUpdateStateUpdates(tweet.tweet_id, tweet.date, manager, rumor_type, type);
			}
		}	
	}
	
	
	private  Long getID(TimeStamp stamp)
	{
		if (rumor_type == 0)
		{
			return idMap.get(stamp.Author);
		}
		else
		{
			return stamp.Author_ID;
		}
	}
	
	
	private boolean hasFollowers(TimeStamp stamp)
	{
		if (rumor_type == 0)
		{
			return followerMap.containsKey(stamp.Author);
		}
		else
		{
			return followerMap.containsKey(Long.toString(stamp.Author_ID));
		}
	}
	
	private  ArrayList<Long> getFollowers(TimeStamp stamp)
	{
		if (rumor_type == 0)
		{
			return followerMap.get(stamp.Author);
		}
		else
		{
			return followerMap.get(Long.toString(stamp.Author_ID));
		}
	}
}
