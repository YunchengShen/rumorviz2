package precompute;

public class TweetInfo {
	public TweetInfo(int type, String author, String tweet) {
		Type = type;
		Author = author;
		Tweet = tweet;
	}
	public int Type;
	public String Author;
	public String Tweet;
}
