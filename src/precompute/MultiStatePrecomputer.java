package precompute;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import db.DBManager;
import db.TimeStamp;

/**
 * Version of StatePrecomputer that has been augmented with extra states that represent one exposure to the rumor, two exposures, or three or more exposures
 * BothExp (exposure to both rumor and debunk) is still regarded as an undifferentiated terminal node just because of the informational complexity of trying
 * to represent the difference between, e.g., exposure-exposure-debunk and exposure-debunk-exposure
 * 
 * @author Sam
 *
 */
public class MultiStatePrecomputer
{

	private DBManager dbm = null;

	private HashMap<String, Long> name_id = null;

	//Hashmaps that track which IDs are in which state at any given point
	private StateSet<Long> spreader = null;
	private StateSet<Long> debunker = null;
	private StateSet<Long> BothTwt = null;
	private StateSet<Long> RumorExp1 = null;
	private StateSet<Long> RumorExp2 = null;
	private StateSet<Long> RumorExp3 = null;
	private StateSet<Long> DebunkExp1 = null;
	private StateSet<Long> DebunkExp2 = null;
	private StateSet<Long> DebunkExp3 = null;
	private StateSet<Long> BothExp = null;
	//private StateSet<Long> AlreadyCounted = null; //I don't know what this is, so I am disabling it

	private HashMap Followers = null;
	public static final int METEOR_TYPE = 0, AP_TYPE = 1;

	//Indices of the various 11 states within the state matrix
	public static final int start_id = 0, BExp_id = 1, RTwt_id = 2, DTwt_id = 3, BTwt_id = 4, 
			RExp_id_1 = 5, RExp_id_2 = 6, RExp_id_3 = 7, DExp_id_1 = 8, DExp_id_2 = 9, DExp_id_3 = 10;

	public static final int passive_id = 0, active_id = 1;
	
	private int population = -1;
	private ArrayList<TimeStamp> TweetTimeline = null;

	private int rumor_type = -1;

	private int start_rumorexp1=0;
	private int start_rumorexp1_arr=0;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception
	{
		System.out.println("Initializing MultiStatePrecomputer...");
		MultiStatePrecomputer sp = new MultiStatePrecomputer(0);
		System.out.println("Precomputing states...");
		sp.precomputeStates();
		System.out.println("Done.");
	}

	public MultiStatePrecomputer(int rumor_type) throws Exception
	{
		System.out.println("Initializing DBManager...");
		this.rumor_type = rumor_type;
		dbm = new DBManager();

		DebunkExp1 = new StateSet<Long>();
		DebunkExp2 = new StateSet<Long>();
		DebunkExp3 = new StateSet<Long>();

		RumorExp1 = new StateSet<Long>();
		RumorExp2 = new StateSet<Long>();
		RumorExp3 = new StateSet<Long>();

		BothExp = new StateSet<Long>();
		spreader = new StateSet<Long>();
		debunker = new StateSet<Long>();
		BothTwt = new StateSet<Long>();
		//AlreadyCounted = new StateSet<Long>(); // In order to prevent multiple count of the same person while Reflexive link calculation //What?
		
		System.out.println("Getting tweeter names");
		name_id = dbm.TweeterNameToID(rumor_type);
		System.out.println("Getting followers");
		Followers = dbm.selectAllFollower(rumor_type);
		if (rumor_type == 0)
			population = 1451046;
		else
			population = 4868413;
	}

	private void precomputeStates() throws Exception
	{
		System.out.println("Retrieving tweet timeline from database...");
		if (rumor_type == 0)
		{
			TweetTimeline = dbm.selectTweetTimeline(0, 1361920704, METEOR_TYPE);
		}
		else
		{
			TweetTimeline = dbm.selectTweetTimeline(-1, -1, AP_TYPE);
		}

		
		//Before we take state transition snapshots from the Tweet Timeline, we have to do one pass over the timeline to 
		//decide which users will ultimately turn out to be active
		System.out.println("Calculating which users are active and which are passive...");
		HashSet<Long> activeUsers = new HashSet<Long>();
		for (TimeStamp timeStamp : TweetTimeline)
		{
			if (name_id.containsKey(timeStamp.Author))
				activeUsers.add(name_id.get(timeStamp.Author));
			
		}
		System.out.println(activeUsers.size() + " active users found...");

		System.out.println("Precomputing state transitions...");
		int[][] NodeWeight = new int[2][11];
		System.out.println(TweetTimeline.size() + " tweets total");
		for (int i = 0; i < TweetTimeline.size(); i++)
		{
			
			if ( i % 100 == 0) System.out.println(i + " tweets processed...");
			
			TimeStamp t = TweetTimeline.get(i);
			//System.out.println("processing tweet " + i + " - " + t.tweet_id + ", " + t.date);
			int[][][] EdgeWeight = new int[2][11][11];
			
			

			//Set passive initial state
			NodeWeight[passive_id][RExp_id_1] = RumorExp1.passive.size();
			NodeWeight[passive_id][RExp_id_2] = RumorExp2.passive.size();
			NodeWeight[passive_id][RExp_id_3] = RumorExp3.passive.size();
			NodeWeight[passive_id][DExp_id_1] = DebunkExp1.passive.size();
			NodeWeight[passive_id][DExp_id_2]= DebunkExp2.passive.size();
			NodeWeight[passive_id][DExp_id_3] = DebunkExp3.passive.size();
			NodeWeight[passive_id][BExp_id] = BothExp.passive.size();
			NodeWeight[passive_id][RTwt_id] = spreader.passive.size();
			NodeWeight[passive_id][DTwt_id] = debunker.passive.size();
			NodeWeight[passive_id][BTwt_id] = BothTwt.passive.size();
			NodeWeight[passive_id][start_id] = population - (NodeWeight[passive_id][1] + NodeWeight[passive_id][2] + NodeWeight[passive_id][3] + NodeWeight[passive_id][4] + NodeWeight[passive_id][5] + 
					NodeWeight[passive_id][6]);			
			dbm.multiUpdateInitState(rumor_type, t.tweet_id, t.date, NodeWeight[passive_id][start_id], NodeWeight[passive_id][BExp_id], NodeWeight[passive_id][RTwt_id], NodeWeight[passive_id][DTwt_id], 
					NodeWeight[passive_id][BTwt_id], NodeWeight[passive_id][RExp_id_1],NodeWeight[passive_id][RExp_id_2], NodeWeight[passive_id][RExp_id_3], NodeWeight[passive_id][DExp_id_1], 
					NodeWeight[passive_id][DExp_id_2], NodeWeight[passive_id][DExp_id_3],passive_id);
			
			//Set active initial state
			NodeWeight[active_id][RExp_id_1] = RumorExp1.active.size();
			NodeWeight[active_id][RExp_id_2] = RumorExp2.active.size();
			NodeWeight[active_id][RExp_id_3] = RumorExp3.active.size();
			NodeWeight[active_id][DExp_id_1] = DebunkExp1.active.size();
			NodeWeight[active_id][DExp_id_2] = DebunkExp2.active.size();
			NodeWeight[active_id][DExp_id_3] = DebunkExp3.active.size();
			NodeWeight[active_id][BExp_id] = BothExp.active.size();
			NodeWeight[active_id][RTwt_id] = spreader.active.size();
			NodeWeight[active_id][DTwt_id] = debunker.active.size();
			NodeWeight[active_id][BTwt_id] = BothTwt.active.size();
			NodeWeight[active_id][start_id] = population - (NodeWeight[active_id][1] + NodeWeight[active_id][2] + NodeWeight[active_id][3] + NodeWeight[active_id][4] + NodeWeight[active_id][5] + 
					NodeWeight[active_id][6]);			
			dbm.multiUpdateInitState(rumor_type, t.tweet_id, t.date, NodeWeight[active_id][start_id], NodeWeight[active_id][BExp_id], NodeWeight[active_id][RTwt_id], NodeWeight[active_id][DTwt_id], 
					NodeWeight[active_id][BTwt_id], NodeWeight[active_id][RExp_id_1],NodeWeight[active_id][RExp_id_2], NodeWeight[active_id][RExp_id_3], NodeWeight[active_id][DExp_id_1], 
					NodeWeight[active_id][DExp_id_2], NodeWeight[active_id][DExp_id_3],active_id);


			
			
			for (int userType =0; userType < 2; userType++) //For both passive (0) and active (1) users
			{
				boolean passive = userType == 0;
				if (t.type == 1) //if the tweet is a rumor
				{
					ArrayList<Long> sf = null;
					if (rumor_type == 0)
					{
						sf = (ArrayList<Long>) Followers.get(t.Author);
					}
					else
					{
						sf = (ArrayList<Long>) Followers.get(Long.toString(t.Author_ID));
					}
					if (sf != null)
					{
						//System.out.println("\t follower size - " + sf.size());
						int DExp_id;
						StateSet<Long> DebunkExp;
						for (int j = 0; j < sf.size(); j++)
						{
							Long id = sf.get(j);
							
							if (!passive && !activeUsers.contains(id)) continue; //Skip passive followers if we are in active mode
							 
							if (DebunkExp1.get(userType).contains(id) || DebunkExp1.get(userType).contains(id) || DebunkExp1.get(userType).contains(id)) //If the follower is in any state of debunk exposure, rumor exposure moves them to both exposure
							{
								if ( DebunkExp1.get(userType).contains(id))
								{
									DebunkExp = DebunkExp1;
									DExp_id = DExp_id_1;
								}
								else if ( DebunkExp2.get(userType).contains(id))
								{
									DebunkExp = DebunkExp2;
									DExp_id = DExp_id_2;
								}
								else 
								{
									DebunkExp = DebunkExp3;
									DExp_id = DExp_id_3;
								}

									
								NodeWeight[userType][BExp_id]++;
								NodeWeight[userType][DExp_id]--;
								EdgeWeight[userType][DExp_id][BExp_id]++;
								BothExp.get(userType).add(id);
								DebunkExp.get(userType).remove(id);
/*								if (AlreadyCounted.get(userType).contains(id) == true)
									AlreadyCounted.get(userType).remove(id);*/
							}
							//If this person has never been exposed to, or tweeted, a rumor or debunk, stick them in the 1 rumor exposure state
							else if (!RumorExp1.get(userType).contains(id) && !RumorExp2.get(userType).contains(id) && !RumorExp3.get(userType).contains(id) && !BothExp.get(userType).contains(id) && 
									!spreader.get(userType).contains(id) && !debunker.get(userType).contains(id)  && !BothTwt.get(userType).contains(id) )
							{
								NodeWeight[userType][RExp_id_1]++;
								NodeWeight[userType][start_id]--;
								EdgeWeight[userType][start_id][RExp_id_1]++;
								RumorExp1.get(userType).add(id);
								start_rumorexp1++;

								
								
							}
							//If the person has been exposed to the rumor once, move them to twice, etc.
							else if (RumorExp1.get(userType).contains(id) /*&& !AlreadyCounted.get(userType).contains(id)*/)
							{
								//AlreadyCounted.get(userType).add(id);
								EdgeWeight[userType][RExp_id_1][RExp_id_2]++;
								RumorExp1.get(userType).remove(id);
								RumorExp2.get(userType).add(id);
							}
							//If the person has been exposed to the rumor once, move them to twice, etc.
							else if (RumorExp2.get(userType).contains(id) /*&& !AlreadyCounted.get(userType).contains(id)*/)
							{
								//AlreadyCounted.get(userType).add(id);
								EdgeWeight[userType][RExp_id_2][RExp_id_3]++;
								RumorExp2.get(userType).remove(id);
								RumorExp3.get(userType).add(id);
							}
							else if (RumorExp3.get(userType).contains(id) /*&& !AlreadyCounted.get(userType).contains(id)*/)
							{
								//AlreadyCounted.get(userType).add(id);
								EdgeWeight[userType][RExp_id_3][RExp_id_3]++;
							}
							else if (BothExp.get(userType).contains(id) /*&& AlreadyCounted.contains(id) == false*/)
							{
/*								AlreadyCounted.add(id);
*/								EdgeWeight[userType][BExp_id][BExp_id]++;
							}
						}
						start_rumorexp1_arr+=EdgeWeight[userType][start_id][RExp_id_1];

					}
					
					Long id;
	
					if (rumor_type == 0)
					{
						id = name_id.get(t.Author);
					}
					else
					{
						id = t.Author_ID;
					}
	
					if (id != null)
					{
						int source = -1, dest = -1;
						StringBuilder builder = new StringBuilder();
						if (RumorExp1.get(userType).contains(id))
						{
							source = RExp_id_1;
							dest = RTwt_id;
							builder.append(RExp_id_1);

						}
						else if (RumorExp2.get(userType).contains(id))
						{
							source = RExp_id_2;
							dest = RTwt_id;
							builder.append(RExp_id_2);


						}
						else if (RumorExp3.get(userType).contains(id))
						{
							source = RExp_id_3;
							dest = RTwt_id;
							builder.append(RExp_id_3);


						}
						else if (DebunkExp1.get(userType).contains(id))
						{
							source = DExp_id_1;
							dest = RTwt_id;
							builder.append(DExp_id_1);


						}
						else if (DebunkExp2.get(userType).contains(id))
						{
							source = DExp_id_2;
							dest = RTwt_id;
							builder.append(DExp_id_2);


						}
						else if (DebunkExp3.get(userType).contains(id))
						{
							source = DExp_id_3;
							dest = RTwt_id;
							builder.append(DExp_id_3);


						}
						else if (BothExp.get(userType).contains(id))
						{
							source = BExp_id;
							dest = RTwt_id;
							builder.append(BExp_id);

						}
						else if (debunker.get(userType).contains(id))
						{
							source = DTwt_id;
							dest = BTwt_id;
							builder.append(DTwt_id);

						}
						else if (!spreader.get(userType).contains(id)  && !BothTwt.get(userType).contains(id))
						{
							source = start_id;
							dest = RTwt_id;
							builder.append(start_id);
							

						}
						else if (spreader.get(userType).contains(id) == true)
						{
							source = RTwt_id;
							dest = RTwt_id;
							builder.append(RTwt_id);

						}
						else if (BothTwt.get(userType).contains(id) == true)
						{
							source = BTwt_id;
							dest = BTwt_id;
							builder.append(BTwt_id);

						}
						if (builder.length() > 1) System.out.println(builder);
	
						if (source != -1 && dest != -1)
						{
							NodeWeight[userType][dest]++;
							NodeWeight[userType][source]--;
							if (source != dest /*|| (source == dest && AlreadyCounted.contains(id) == false)*/)
							{
								EdgeWeight[userType][source][dest]++;
		/*						if (source == dest)
									AlreadyCounted.add(id);*/
							}
							if (source == DTwt_id && dest == BTwt_id)
							{
								BothTwt.get(userType).add(id);
								debunker.get(userType).remove(id);
/*								if (AlreadyCounted.contains(id) == true)
									AlreadyCounted.remove(id);*/
							}
							else if (dest == RTwt_id)
							{
								if (source != RTwt_id)
									spreader.get(userType).add(id);
	
								if (source == RExp_id_1)
									RumorExp1.get(userType).remove(id);
								else if (source == RExp_id_2)
									RumorExp2.get(userType).remove(id);
								else if (source == RExp_id_3)
									RumorExp3.get(userType).remove(id);
								else if (source == DExp_id_1)
									DebunkExp1.get(userType).remove(id);
								else if (source == DExp_id_2)
									DebunkExp2.get(userType).remove(id);
								else if (source == DExp_id_3)
									DebunkExp3.get(userType).remove(id);
								else if (source == BExp_id)
									BothExp.get(userType).remove(id);
							}
							else if (source == BTwt_id && dest == BTwt_id)
							{
								;
							}
						}
					}
				}
				else if (t.type == 2) // if the tweet is a debunk
				{
					ArrayList<Long> df = null;
	
					if (rumor_type == 0)
					{
						df = (ArrayList<Long>) Followers.get(t.Author);
					}
					else
					{
						df = (ArrayList<Long>) Followers.get(Long.toString(t.Author_ID));
					}
	
					if (df != null)
					{
					//	System.out.println("\t follower size - " + df.size());
						int RExp_id;
						StateSet<Long> RumorExp;
						for (int j = 0; j < df.size(); j++)
						{
							Long id = df.get(j);
							if (!passive && !activeUsers.contains(id)) continue; //Skip passive followers if we are in active mode

							if (RumorExp1.get(userType).contains(id) || RumorExp2.get(userType).contains(id) || RumorExp3.get(userType).contains(id))
							{
							
								if ( RumorExp1.get(userType).contains(id))
								{
									RumorExp = RumorExp1;
									RExp_id = RExp_id_1;
								}
								else if ( RumorExp2.get(userType).contains(id))
								{
									RumorExp = RumorExp2;
									RExp_id = RExp_id_2;
								}
								else 
								{
									RumorExp = RumorExp3;
									RExp_id = RExp_id_3;
								}
							
							
								NodeWeight[userType][BExp_id]++;
								NodeWeight[userType][RExp_id]--;
								EdgeWeight[userType][RExp_id][BExp_id]++;
								BothExp.get(userType).add(id);
								RumorExp.get(userType).remove(id);
/*								if (AlreadyCounted.contains(id) == true)
									AlreadyCounted.remove(id);*/
							}
							else if (!DebunkExp1.get(userType).contains(id) && !DebunkExp2.get(userType).contains(id) && !DebunkExp3.get(userType).contains(id)  &&
									!BothExp.get(userType).contains(id)  && !spreader.get(userType).contains(id)  && !debunker.get(userType).contains(id)  && !BothTwt.get(userType).contains(id) )
							{
								NodeWeight[userType][DExp_id_1]++;
								NodeWeight[userType][start_id]--;
								EdgeWeight[userType][start_id][DExp_id_1]++;
								DebunkExp1.get(userType).add(id);
							}
							else if (DebunkExp1.get(userType).contains(id)  /*&& AlreadyCounted.contains(id) == false*/)
							{
/*								AlreadyCounted.add(id);
*/								EdgeWeight[userType][DExp_id_1][DExp_id_2]++;
								DebunkExp1.get(userType).remove(id);
								DebunkExp2.get(userType).add(id);
							}
							else if (DebunkExp2.get(userType).contains(id)  /*&& AlreadyCounted.contains(id) == false*/)
							{
/*								AlreadyCounted.add(id);
*/								EdgeWeight[userType][DExp_id_2][DExp_id_3]++;
								DebunkExp2.get(userType).remove(id);
								DebunkExp3.get(userType).add(id);
							}
							else if (DebunkExp3.get(userType).contains(id)  /*&& AlreadyCounted.contains(id) == false*/)
							{
/*								AlreadyCounted.add(id);
*/								EdgeWeight[userType][DExp_id_3][DExp_id_3]++;
							}
							else if (BothExp.get(userType).contains(id)  /*&& AlreadyCounted.contains(id) == false*/)
							{
/*								AlreadyCounted.add(id);
*/								EdgeWeight[userType][BExp_id][BExp_id]++;
							}
						}
					}
					Long id;
	
					if (rumor_type == 0)
					{
						id = name_id.get(t.Author);
					}
					else
					{
						id = t.Author_ID;
					}
	
					if (id != null)
					{
						int source = -1, dest = -1;
						if (RumorExp1.get(userType).contains(id))
						{
							source = RExp_id_1;
							dest = DTwt_id;
						}
						else if (RumorExp2.get(userType).contains(id))
						{
							source = RExp_id_2;
							dest = DTwt_id;
						}
						else if (RumorExp3.get(userType).contains(id))
						{
							source = RExp_id_3;
							dest = DTwt_id;
						}
						else if (DebunkExp1.get(userType).contains(id))
						{
							source = DExp_id_1;
							dest = DTwt_id;
						}
						else if (DebunkExp2.get(userType).contains(id))
						{
							source = DExp_id_2;
							dest = DTwt_id;
						}
						else if (DebunkExp3.get(userType).contains(id))
						{
							source = DExp_id_3;
							dest = DTwt_id;
						}
						else if (BothExp.get(userType).contains(id))
						{
							source = BExp_id;
							dest = DTwt_id;
						}
						else if (spreader.get(userType).contains(id))
						{
							source = RTwt_id;
							dest = BTwt_id;
						}
						else if (debunker.get(userType).contains(id) == false && BothTwt.get(userType).contains(id) == false)
						{
							source = start_id;
							dest = DTwt_id;
						}
						else if (debunker.get(userType).contains(id) == true)
						{
							source = DTwt_id;
							dest = DTwt_id;
						}
						else if (BothTwt.get(userType).contains(id) == true)
						{
							source = BTwt_id;
							dest = BTwt_id;
						}
	
						if (source != -1 && dest != -1)
						{
							NodeWeight[userType][dest]++;
							NodeWeight[userType][source]--;
							if (source != dest /*|| (source == dest && AlreadyCounted.contains(id) == false)*/)
							{
								EdgeWeight[userType][source][dest]++;
/*								if (source == dest)
									AlreadyCounted.add(id);*/
							}
							if (source == RTwt_id && dest == BTwt_id)
							{
								BothTwt.get(userType).add(id);
								spreader.get(userType).remove(id);
/*								if (AlreadyCounted.contains(id) == true)
									AlreadyCounted.remove(id);*/
							}
							else if (dest == DTwt_id)
							{
								if (source != DTwt_id)
									debunker.get(userType).add(id);
	
								if (source == RExp_id_1)
									RumorExp1.get(userType).remove(id);
								else if (source == RExp_id_2)
									RumorExp2.get(userType).remove(id);
								else if (source == RExp_id_3)
									RumorExp3.get(userType).remove(id);
								else if (source == DExp_id_1)
									DebunkExp1.get(userType).remove(id);
								else if (source == DExp_id_2)
									DebunkExp2.get(userType).remove(id);
								else if (source == DExp_id_3)
									DebunkExp3.get(userType).remove(id);
								else if (source == BExp_id)
									BothExp.get(userType).remove(id);
							}
							else if (source == BTwt_id && dest == BTwt_id)
							{
								;
							}
						}
					}
				}
				if (rumor_type == 0)
				{
					dbm.multiUpdateStateUpdates(t.tweet_id, t.date, EdgeWeight[userType], METEOR_TYPE, userType);
					//dbm.updateReflexiveUpdates(t.tweet_id, EdgeWeight, METEOR_TYPE);
				}
				else
				{
					dbm.updateStateUpdates(t.tweet_id, t.date, EdgeWeight[userType], AP_TYPE);
					//dbm.updateReflexiveUpdates(t.tweet_id, EdgeWeight, AP_TYPE);
				}
			}
		}
		System.out.println(start_rumorexp1 + " instances of moving from start stage to first level of rumor exposure...");
		System.out.println(start_rumorexp1_arr + " instances encoded in EdgeWeight array");

	}
}
