package precompute;

public class AuthorInfo {

	public AuthorInfo(int followers, int followees, int tRCount, int tDCount,
			int rECount, int dECount, String ScreenName) {
		this.followers = followers;
		this.followees = followees;
		TRCount = tRCount;
		TDCount = tDCount;
		RECount = rECount;
		DECount = dECount;
		this.ScreenName = ScreenName;
	}

	public String ScreenName;
	public int followers;
	public int followees;
	public int TRCount;
	public int TDCount;
	public int RECount;
	public int DECount;
}
