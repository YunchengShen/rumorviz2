package precompute;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import db.DBManager;
import db.TimeStamp;

public class StatePrecomputer {
	private DBManager dbm = null;
	private HashSet<Long> spreader = null;
	private HashSet<Long> debunker = null;
	private HashSet<Long> BothTwt = null;
	private HashMap<String,Long> name_id = null;
	private HashSet<Long> RumorExp = null;
	private HashSet<Long> DebunkExp = null;
	private HashSet<Long> BothExp = null;
	private HashSet<Long> AlreadyCounted = null;
	private HashMap Followers = null;
	public static final int METEOR_TYPE = 0, AP_TYPE = 1;
	public static final int start_id = 0, RExp_id = 1, DExp_id = 2, BExp_id = 3, 
			RTwt_id = 4, DTwt_id = 5, BTwt_id = 6;
	private int population = -1;
	private ArrayList<TimeStamp> TweetTimeline = null;
	
	private int rumor_type = -1;
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		StatePrecomputer sp = new StatePrecomputer(1);
		sp.precomputeStates();
	}
	
	public StatePrecomputer(int rumor_type){
		this.rumor_type = rumor_type;
		dbm = new DBManager();
		DebunkExp = new HashSet<Long>();
		RumorExp = new HashSet<Long>();
		BothExp = new HashSet<Long>();
		spreader = new HashSet<Long>();
		debunker = new HashSet<Long>();
		BothTwt = new HashSet<Long>();
		AlreadyCounted = new HashSet<Long>();		// In order to prevent multiple count of the same person while Reflexive link calculation
		name_id = dbm.TweeterNameToID(rumor_type);
		Followers = dbm.selectAllFollower(rumor_type);
		if(rumor_type == 0)
			population = 1451046;
		else 
			population = 4868413;
	}

	private void precomputeStates(){
		if(rumor_type == 0){
			TweetTimeline = dbm.selectTweetTimeline(0, 1361920704, METEOR_TYPE);			
		} else {
			TweetTimeline = dbm.selectTweetTimeline(-1, -1, AP_TYPE);			
		}
		int [] NodeWeight = new int [7];
		
		for(int i = 0;  i < TweetTimeline.size(); i++){
			TimeStamp t = TweetTimeline.get(i);
			System.out.println("processing tweet " + i + " - " + t.tweet_id + ", " + t.date);
			int [][] EdgeWeight = new int [7][7];
			
			NodeWeight[RExp_id] = RumorExp.size(); NodeWeight[DExp_id] = DebunkExp.size(); NodeWeight[BExp_id] = BothExp.size();
			NodeWeight[RTwt_id] = spreader.size(); NodeWeight[DTwt_id] = debunker.size(); NodeWeight[BTwt_id] = BothTwt.size();
			NodeWeight[start_id] =  population - (NodeWeight[1] + NodeWeight[2] + NodeWeight[3] + NodeWeight[4] + NodeWeight[5] + NodeWeight[6]);
			
			dbm.updateInitState(rumor_type, t.tweet_id, t.date, NodeWeight[start_id], NodeWeight[RExp_id], NodeWeight[DExp_id],
					NodeWeight[BExp_id], NodeWeight[RTwt_id], NodeWeight[DTwt_id], NodeWeight[BTwt_id]);
			
			if(t.type == 1){
				ArrayList<Long> sf= null;
				if(rumor_type == 0) {
					sf = (ArrayList<Long>) Followers.get(t.Author);					
				} else {
					sf = (ArrayList<Long>) Followers.get(Long.toString(t.Author_ID));
				}
				if(sf != null){
					System.out.println("\t follower size - " + sf.size());
					for(int j = 0; j < sf.size(); j++){
						Long id = sf.get(j);
						if(DebunkExp.contains(id) == true){
							NodeWeight[BExp_id]++;
							NodeWeight[DExp_id]--;
							EdgeWeight[DExp_id][BExp_id]++;
							BothExp.add(id);
							DebunkExp.remove(id);
							if(AlreadyCounted.contains(id) == true) AlreadyCounted.remove(id);
						} else if(RumorExp.contains(id) == false && BothExp.contains(id) == false && spreader.contains(id) == false
								&& debunker.contains(id) == false && BothTwt.contains(id) == false){
							NodeWeight[RExp_id]++;
							NodeWeight[start_id]--;
							EdgeWeight[start_id][RExp_id]++;
							RumorExp.add(id);
						} else if(RumorExp.contains(id) == true && AlreadyCounted.contains(id) == false){
							AlreadyCounted.add(id);
							EdgeWeight[RExp_id][RExp_id]++;
						} else if(BothExp.contains(id) == true && AlreadyCounted.contains(id) == false){
							AlreadyCounted.add(id);
							EdgeWeight[BExp_id][BExp_id]++;
						}
					}
				}
				Long id;
				
				if(rumor_type == 0){
					id = name_id.get(t.Author);					
				} else {
					id = t.Author_ID;
				}
				
				if(id != null){
					int source = -1, dest = -1;
					if(RumorExp.contains(id)){
						source = RExp_id; dest = RTwt_id;
					} else if(DebunkExp.contains(id)){
						source = DExp_id; dest = RTwt_id;
					} else if(BothExp.contains(id)){
						source = BExp_id; dest = RTwt_id;
					} else if(debunker.contains(id)) {
						source = DTwt_id; dest = BTwt_id;	
					} else if(spreader.contains(id) == false && BothTwt.contains(id) == false){
						source = start_id; dest = RTwt_id;
					} else if(spreader.contains(id) == true){
						source = RTwt_id; dest = RTwt_id;
					} else if(BothTwt.contains(id) == true){
						source = BTwt_id; dest = BTwt_id;
					}
					
					if(source != -1 && dest != -1){
						NodeWeight[dest]++;
						NodeWeight[source]--;
						if(source != dest || (source == dest && AlreadyCounted.contains(id) == false)){
							EdgeWeight[source][dest]++;
							if(source == dest) 	AlreadyCounted.add(id);
						}
						if(source == DTwt_id && dest == BTwt_id){
							BothTwt.add(id);
							debunker.remove(id);
							if(AlreadyCounted.contains(id) == true) AlreadyCounted.remove(id);
						} else if(dest == RTwt_id){
							if(source != RTwt_id)
								spreader.add(id);
								
							if(source == RExp_id)
								RumorExp.remove(id);
							else if(source == DExp_id)
								DebunkExp.remove(id);
							else if(source == BExp_id)
								BothExp.remove(id);
						} else if(source == BTwt_id && dest == BTwt_id){
							;
						}
					}
				}
			}
			else if(t.type == 2){
				ArrayList<Long> df = null;
				
				if(rumor_type == 0){
					df = (ArrayList<Long>) Followers.get(t.Author);					
				} else {
					df = (ArrayList<Long>) Followers.get(Long.toString(t.Author_ID));
				}
				
				if(df != null) {
					System.out.println("\t follower size - " + df.size());
					for(int j = 0; j < df.size(); j++){
						Long id = df.get(j);
						if(RumorExp.contains(id) == true){
							NodeWeight[BExp_id]++;
							NodeWeight[RExp_id]--;
							EdgeWeight[RExp_id][BExp_id]++;
							BothExp.add(id);
							RumorExp.remove(id);
							if(AlreadyCounted.contains(id) == true) AlreadyCounted.remove(id);
						} else if(DebunkExp.contains(id) == false && BothExp.contains(id) == false && spreader.contains(id) == false
								&& debunker.contains(id) == false && BothTwt.contains(id) == false){
							NodeWeight[DExp_id]++;
							NodeWeight[start_id]--;
							EdgeWeight[start_id][DExp_id]++;
							DebunkExp.add(id);
						} else if(DebunkExp.contains(id) == true && AlreadyCounted.contains(id) == false){
							AlreadyCounted.add(id);
							EdgeWeight[DExp_id][DExp_id]++;
						} else if(BothExp.contains(id) == true && AlreadyCounted.contains(id) == false){
							AlreadyCounted.add(id);
							EdgeWeight[BExp_id][BExp_id]++;
						}
					}
				}
				Long id;
				
				if(rumor_type == 0){
					id = name_id.get(t.Author);					
				} else {
					id = t.Author_ID;
				}
				
				if(id != null){
					int source = -1, dest = -1;
					if(RumorExp.contains(id)){
						source = RExp_id; dest = DTwt_id;
					} else if(DebunkExp.contains(id)){
						source = DExp_id; dest = DTwt_id;
					} else if(BothExp.contains(id)){
						source = BExp_id; dest = DTwt_id;
					} else if(spreader.contains(id)) {
						source = RTwt_id; dest = BTwt_id;	
					} else if(debunker.contains(id) == false && BothTwt.contains(id) == false){
						source = start_id; dest = DTwt_id;
					} else if(debunker.contains(id) == true){
						source = DTwt_id; dest = DTwt_id;
					} else if(BothTwt.contains(id) == true){
						source = BTwt_id; dest = BTwt_id;
					}
					
					if(source != -1 && dest != -1){
						NodeWeight[dest]++;
						NodeWeight[source]--;
						if(source != dest || (source == dest && AlreadyCounted.contains(id) == false)){
							EdgeWeight[source][dest]++;
							if(source == dest) 	AlreadyCounted.add(id);
						}
						if(source == RTwt_id && dest == BTwt_id){
							BothTwt.add(id);
							spreader.remove(id);
							if(AlreadyCounted.contains(id) == true) AlreadyCounted.remove(id);
						} else if(dest == DTwt_id){
							if(source != DTwt_id)
								debunker.add(id);
							
							if(source == RExp_id)
								RumorExp.remove(id);
							else if(source == DExp_id)
								DebunkExp.remove(id);
							else if(source == BExp_id)
								BothExp.remove(id);
						} else if(source == BTwt_id && dest == BTwt_id){
							;
						}
					}
				}
			}
			if(rumor_type == 0){
				dbm.updateStateUpdates(t.tweet_id, t.date, EdgeWeight, METEOR_TYPE);
				dbm.updateReflexiveUpdates(t.tweet_id, EdgeWeight, METEOR_TYPE);				
			} else {
				dbm.updateStateUpdates(t.tweet_id, t.date, EdgeWeight, AP_TYPE);
				dbm.updateReflexiveUpdates(t.tweet_id, EdgeWeight, AP_TYPE);				
			}
		}
	}
}
	
