package precompute;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;

import precompute.StateManager.State;

/**
 * This is a class which is meant to encode a state transition diagram that represents
 * the movement of Twitter users between states of interaction with a tweets that either 
 * propagate or debunk a rumor.
 * 
 * So for example, a Twitter user might start of in the start state, indicating that they
 * have neither been exposed to or propagated the rumor or the debunk. If someone they follow
 * tweets or retweets an instance of the rumor, than that user moves to the "exposed once" state.
 * 
 * The class differentiates between "active" users who at some point tweet or retweet something,
 * and "passive" users who are just exposed to stuff
 * @author Sam
 *
 */
public class StateManager
{
	public enum State //Types of states of interaction with rumor
	{
		start (0, "Start"),			//Not exposed to rumor or debunk, has not tweeted or retweeted either
		rumor_exp_1 (1, "RumorExp1"),	//Exposed to rumor exactly once
		rumor_exp_2 (2, "RumorExp2"),
		rumor_exp_3m (3, "RumorExp3m"),	//Exposed to rumor 3 times or more
		rumor_twt_1 (4,"RumorTwt1"),	//Has retweeted rumor exactly once
		rumor_twt_2 (5,"RumorTwt2"),
		rumor_twt_3m (6,"RumorTwt3m"),
		debunk_exp_1 (7,"DebunkExp1"),	//Exposed to debunk exactly once
		debunk_exp_2 (8,"DebunkExp2"),
		debunk_exp_3m (9,"DebunkExp3m"),	//Exposed to debunk 3 times or more
		debunk_twt_1 (10,"DebunkTwt1"),	//Has retweeted debunk exactly once
		debunk_twt_2 (11,"DebunkTwt2"),
		debunk_twt_3m (12,"DebunkTwt3m"),
		both_exp (13,"BothExp"),		//Has been exposed to both rumor and debunk any number of times
		both_twt (14,"BothTwt");		//Has tweeted or retweeted both rumor and debunk any number of times
		
		public final int index; //This may not be necessary
		public final String column; //MySQL column name that this state corresponds to
		
		State (int index, String column)
		{
			this.index = index;
			this.column = column;
		}
	}
	public enum UserType //Types of users
	{
		passive (0), 
		active (1);
		
		public final int flag; //What these values map to in the DB
		UserType (int flag)
		{
			this.flag = flag;
		}
		
	}
	
	public enum TweetType //Tweet types
	{
		rumor, debunk
	}
	
	public enum Relation //Ways a user can be related to a given tweet (either exposed to it or tweeted it)
	{
		exp, twt;
	}
	
	public static LinkedHashMap<State,LinkedHashMap<TweetType, LinkedHashMap<Relation, State>>> transitionMap = enumerateTransitionMap(); //Map of what type of tweet leads from each state to its neighbors
	public static LinkedHashMap<State,LinkedHashSet<State>> possibleTransitions = enumeratePossibleTransitions(); //Edge map of state transition diagram
	
	
	//All state-tracking data structures are split by type- active users and passive users
	private HashMap<Long,State> memberMap; //Records exactly who is in what state. We don't bother storing everyone who is in start state,
											//we just assume anyone not in any other state is in the start state. Updated with each new tweet.	
	private LinkedHashMap<UserType,LinkedHashMap<State,Integer>> countMap; //Records how many people are in each state. Gets updated with each tweet.
	
	private LinkedHashMap<UserType,LinkedHashMap<State, LinkedHashMap<State, Integer>>> moveMap; //Records how many people have moved between each state. It is recreated for each new tweet. 
	
	public StateManager(int numPassiveUsers, int numActiveUsers)
	{
		memberMap = new HashMap<Long,State>();
		
		countMap = new LinkedHashMap<UserType, LinkedHashMap<State,Integer>>();
		moveMap = new LinkedHashMap<StateManager.UserType, LinkedHashMap<State,LinkedHashMap<State,Integer>>>();
		for (UserType type: UserType.values())
		{
			countMap.put(type, new LinkedHashMap<State, Integer>());
			moveMap.put(type, new LinkedHashMap<StateManager.State, LinkedHashMap<State,Integer>>());
			for (State state : State.values())
			{
				countMap.get(type).put(state, 0);
				moveMap.get(type).put(state, new LinkedHashMap<StateManager.State, Integer>());
			}
		}
		countMap.get(UserType.passive).put(State.start, numPassiveUsers);
		countMap.get(UserType.active).put(State.start, numActiveUsers);
	}
	
	

	/**
	 * List the allowable transitions and return them as a LinkedHashMap of States to LinkedHashSets of states. Enumerates this by condensing transitionMap
	 * @return
	 */
	private static LinkedHashMap<State,LinkedHashSet<State>> enumeratePossibleTransitions()
	{
		LinkedHashMap<State,LinkedHashSet<State>> transitions = new LinkedHashMap<State,LinkedHashSet<State>>();
		LinkedHashMap<State, LinkedHashMap<TweetType, LinkedHashMap<Relation, State>>> transitionMap = enumerateTransitionMap();

		
		for (State state : State.values())
		{
			transitions.put(state,new LinkedHashSet<State>());
		}
		
		for (State source : transitionMap.keySet())
		{
			for (TweetType tweet : transitionMap.get(source).keySet())
			{
				for (Relation relate : transitionMap.get(source).get(tweet).keySet())
				{
					transitions.get(source).add(transitionMap.get(source).get(tweet).get(relate));
				}
			}
		}
		
		return transitions;
	}
	
	public int getCount(State state, UserType type)
	{
		return countMap.get(type).get(state);
	}


	public LinkedHashMap<UserType, LinkedHashMap<State, LinkedHashMap<State, Integer>>> getMoveMap()
	{
		return moveMap;
	}


	/**
	 * Clears moveMap. Have to do this before recording results of each tweet
	 */
	public void clearUpdates()
	{
		for (UserType type : UserType.values())
		{	
			for (State source : possibleTransitions.keySet())
			{
				for (State dest: possibleTransitions.get(source))
				{
					moveMap.get(type).get(source).put(dest, 0);
				}
			}
		}
		
	}

	public State getCurrentState(Long authorID)
	{
		if (memberMap.containsKey(authorID)) 
			return memberMap.get(authorID);
		else
			return State.start;
	}

	/**
	 * Moves a user from one state to another. Updates counts and updates. 
	 * @param destination
	 * @param userType 
	 */
	public void moveUserToState(Long authorID, State destination, UserType userType)
	{
		if (destination != null)
		{
			State source = getCurrentState (authorID);
			memberMap.put(authorID, destination);
			countMap.get(userType).put(source, countMap.get(userType).get(source)-1);
			countMap.get(userType).put(destination, countMap.get(userType).get(destination)+1);
			moveMap.get(userType).get(source).put(destination, moveMap.get(userType).get(source).get(destination)+1);
		}
	}
	
	/**
	 * Enumerates the state transitions in the diagram, as well as what circumstances lead to each transition. Basically a schematic of the diagram. 
	 * @return
	 */
	private static LinkedHashMap<State, LinkedHashMap<TweetType, LinkedHashMap<Relation, State>>> enumerateTransitionMap()
	{
		LinkedHashMap<State, LinkedHashMap<TweetType, LinkedHashMap<Relation, State>>> transitionMap = new LinkedHashMap<StateManager.State, LinkedHashMap<TweetType,LinkedHashMap<Relation,State>>>();
		
		//Initialize inner data structures
		for(State state : State.values())
		{
			transitionMap.put(state, new LinkedHashMap<StateManager.TweetType, LinkedHashMap<Relation,State>>());
			for (TweetType tweet : TweetType.values())
			{
				transitionMap.get(state).put(tweet, new LinkedHashMap<StateManager.Relation, StateManager.State>());
			}
		}
		
		//Transitions from the start state
		transitionMap.get(State.start).get(TweetType.rumor).put(Relation.exp,State.rumor_exp_1);//For example, when in State.start, being exposed (Relate.exp) to a rumor (Tweet.rumor) leads to State.rumor_exp_1
		transitionMap.get(State.start).get(TweetType.rumor).put(Relation.twt,State.rumor_twt_1);
		transitionMap.get(State.start).get(TweetType.debunk).put(Relation.exp,State.debunk_exp_1);
		transitionMap.get(State.start).get(TweetType.debunk).put(Relation.twt,State.debunk_twt_1);
		
		//transitionMap from a state of rumor exposure or tweeting
		transitionMap.get(State.rumor_exp_1).get(TweetType.rumor).put(Relation.exp,State.rumor_exp_2);
		transitionMap.get(State.rumor_exp_1).get(TweetType.rumor).put(Relation.twt,State.rumor_twt_1);
		transitionMap.get(State.rumor_exp_1).get(TweetType.debunk).put(Relation.twt,State.debunk_twt_1);
		transitionMap.get(State.rumor_exp_1).get(TweetType.debunk).put(Relation.exp,State.both_exp);
		
		transitionMap.get(State.rumor_exp_2).get(TweetType.rumor).put(Relation.exp,State.rumor_exp_3m);
		transitionMap.get(State.rumor_exp_2).get(TweetType.rumor).put(Relation.twt,State.rumor_twt_1);
		transitionMap.get(State.rumor_exp_2).get(TweetType.debunk).put(Relation.twt,State.debunk_twt_1);
		transitionMap.get(State.rumor_exp_2).get(TweetType.debunk).put(Relation.exp,State.both_exp);
		
		transitionMap.get(State.rumor_exp_3m).get(TweetType.rumor).put(Relation.exp,State.rumor_exp_3m);
		transitionMap.get(State.rumor_exp_3m).get(TweetType.rumor).put(Relation.twt,State.rumor_twt_1);
		transitionMap.get(State.rumor_exp_3m).get(TweetType.debunk).put(Relation.twt,State.debunk_twt_1);
		transitionMap.get(State.rumor_exp_3m).get(TweetType.debunk).put(Relation.exp,State.both_exp);

		transitionMap.get(State.rumor_twt_1).get(TweetType.rumor).put(Relation.twt,State.rumor_twt_2);
		transitionMap.get(State.rumor_twt_1).get(TweetType.debunk).put(Relation.twt,State.both_twt);
		
		transitionMap.get(State.rumor_twt_2).get(TweetType.rumor).put(Relation.twt,State.rumor_twt_3m);
		transitionMap.get(State.rumor_twt_2).get(TweetType.debunk).put(Relation.twt,State.both_twt);

		transitionMap.get(State.rumor_twt_3m).get(TweetType.rumor).put(Relation.twt,State.rumor_twt_3m);
		transitionMap.get(State.rumor_twt_3m).get(TweetType.debunk).put(Relation.twt,State.both_twt);	
		
		//transitionMap from a state of debunk exposure or tweeting
		transitionMap.get(State.debunk_exp_1).get(TweetType.debunk).put(Relation.exp,State.debunk_exp_2);
		transitionMap.get(State.debunk_exp_1).get(TweetType.debunk).put(Relation.twt,State.debunk_twt_1);
		transitionMap.get(State.debunk_exp_1).get(TweetType.rumor).put(Relation.twt,State.rumor_twt_1);
		transitionMap.get(State.debunk_exp_1).get(TweetType.rumor).put(Relation.exp,State.both_exp);
		
		transitionMap.get(State.debunk_exp_2).get(TweetType.debunk).put(Relation.exp,State.debunk_exp_3m);
		transitionMap.get(State.debunk_exp_2).get(TweetType.debunk).put(Relation.twt,State.debunk_twt_1);
		transitionMap.get(State.debunk_exp_2).get(TweetType.rumor).put(Relation.twt,State.rumor_twt_1);
		transitionMap.get(State.debunk_exp_2).get(TweetType.rumor).put(Relation.exp,State.both_exp);
		
		transitionMap.get(State.debunk_exp_3m).get(TweetType.debunk).put(Relation.exp,State.debunk_exp_3m);
		transitionMap.get(State.debunk_exp_3m).get(TweetType.debunk).put(Relation.twt,State.debunk_twt_1);
		transitionMap.get(State.debunk_exp_3m).get(TweetType.rumor).put(Relation.twt,State.rumor_twt_1);
		transitionMap.get(State.debunk_exp_3m).get(TweetType.rumor).put(Relation.exp,State.both_exp);

		transitionMap.get(State.debunk_twt_1).get(TweetType.debunk).put(Relation.twt,State.debunk_twt_2);
		transitionMap.get(State.debunk_twt_1).get(TweetType.rumor).put(Relation.twt,State.both_twt);
		
		transitionMap.get(State.debunk_twt_2).get(TweetType.debunk).put(Relation.twt,State.debunk_twt_3m);
		transitionMap.get(State.debunk_twt_2).get(TweetType.rumor).put(Relation.twt,State.both_twt);

		transitionMap.get(State.debunk_twt_3m).get(TweetType.debunk).put(Relation.twt,State.debunk_twt_3m);
		transitionMap.get(State.debunk_twt_3m).get(TweetType.rumor).put(Relation.twt,State.both_twt);
		
		
		transitionMap.get(State.both_exp).get(TweetType.rumor).put(Relation.twt,State.rumor_twt_1);
		transitionMap.get(State.both_exp).get(TweetType.debunk).put(Relation.twt,State.debunk_twt_1);
		transitionMap.get(State.both_exp).get(TweetType.rumor).put(Relation.exp,State.both_exp);
		transitionMap.get(State.both_exp).get(TweetType.debunk).put(Relation.exp,State.both_exp);

		transitionMap.get(State.both_twt).get(TweetType.rumor).put(Relation.twt,State.both_twt);
		transitionMap.get(State.both_twt).get(TweetType.debunk).put(Relation.twt,State.both_twt);

		
		return transitionMap;
	}

}
