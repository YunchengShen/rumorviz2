package precompute;

public class InitStateME {
	public int RumorExp1, RumorExp2, RumorExp3m, DebunkExp1, DebunkExp2, DebunkExp3m;
	public static String [] StateNames = {"Start", "RumorExp1", "RumorExp2", "RumorExp3m", "DebunkExp1", 
		"DebunkExp2", "DebunkExp3m", "BothExp", "RumorTwt", "DebunkTwt", "BothTwt"};
	public int [] States;
	
	public InitStateME(){
		States = new int[StateNames.length];
	}
	
	public void setValue(int StateIndex, int value){
		States[StateIndex] = value;
	}
}
