package precompute;

public class StateUpdate {
	public StateUpdate(long date, int start_RumorExp, int start_DebunkExp,
			int start_RumorTwt, int start_DebunkTwt, int rumorExp_BothExp,
			int rumorExp_RumorTwt, int rumorExp_DebunkTwt,
			int debunkExp_BothExp, int debunkExp_RumorTwt,
			int debunkExp_DebunkTwt, int bothExp_RumorTwt,
			int bothExp_DebunkTwt, int rumorTwt_BothTwt, int debunkTwt_BothTwt,
			int rumorExp_RumorExp, int debunkExp_DebunkExp,
			int bothExp_BothExp, int rumorTwt_RumorTwt,
			int debunkTwt_DebunkTwt, int bothTwt_BothTwt) {
		Date = date;
		Start_RumorExp = start_RumorExp;
		Start_DebunkExp = start_DebunkExp;
		Start_RumorTwt = start_RumorTwt;
		Start_DebunkTwt = start_DebunkTwt;
		RumorExp_BothExp = rumorExp_BothExp;
		RumorExp_RumorTwt = rumorExp_RumorTwt;
		RumorExp_DebunkTwt = rumorExp_DebunkTwt;
		DebunkExp_BothExp = debunkExp_BothExp;
		DebunkExp_RumorTwt = debunkExp_RumorTwt;
		DebunkExp_DebunkTwt = debunkExp_DebunkTwt;
		BothExp_RumorTwt = bothExp_RumorTwt;
		BothExp_DebunkTwt = bothExp_DebunkTwt;
		RumorTwt_BothTwt = rumorTwt_BothTwt;
		DebunkTwt_BothTwt = debunkTwt_BothTwt;
		RumorExp_RumorExp = rumorExp_RumorExp;
		DebunkExp_DebunkExp = debunkExp_DebunkExp;
		BothExp_BothExp = bothExp_BothExp;
		RumorTwt_RumorTwt = rumorTwt_RumorTwt;
		DebunkTwt_DebunkTwt = debunkTwt_DebunkTwt;
		BothTwt_BothTwt = bothTwt_BothTwt;
	}
	public	long	Date;
	public	int	Start_RumorExp;
	public	int	Start_DebunkExp;
	public	int	Start_RumorTwt;
	public	int	Start_DebunkTwt;
	public	int	RumorExp_BothExp;
	public	int	RumorExp_RumorTwt;
	public	int	RumorExp_DebunkTwt;
	public	int	DebunkExp_BothExp;
	public	int	DebunkExp_RumorTwt;
	public	int	DebunkExp_DebunkTwt;
	public	int	BothExp_RumorTwt;
	public	int	BothExp_DebunkTwt;
	public	int	RumorTwt_BothTwt;
	public	int	DebunkTwt_BothTwt;
	public  int RumorExp_RumorExp;
	public	int DebunkExp_DebunkExp;
	public	int BothExp_BothExp;
	public	int RumorTwt_RumorTwt;
	public	int DebunkTwt_DebunkTwt;
	public	int	BothTwt_BothTwt;
}
