package precompute;

public class TweetsOfNetwork {
	public TweetsOfNetwork(String tweetsOfFollowers, String tweetsOfFollowees, String tweetsOfAuthors) {
		TweetsOfFollowers = tweetsOfFollowers;
		TweetsOfFollowees = tweetsOfFollowees;
		TweetsOfAuthors = tweetsOfAuthors;
	}
	public String TweetsOfFollowers;		// Among those made after this tweet, tweets of Followers 
	public String TweetsOfFollowees;		// Among those made after this tweet, tweets of Followees
	public String TweetsOfAuthors;
}
