package precompute;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import db.DBManager;
import db.TimeStamp;

public class TweetNetwork {

	/**
	 * @param args
	 */
	private DBManager dbm;
	private ArrayList<TimeStamp> TweetInfo = null;
	private HashMap<String,HashMap<Long,Object>> Followers = null;
	private HashMap<String,HashMap<Long,Object>> Followees = null;
	private HashMap<String,Long> NameToID = null;

	private int rumor_type = -1;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		TweetNetwork ion = new TweetNetwork(1);
		ion.getTweets();
//		ion.updateTweetsOfFollowers();					
//		ion.updateTweetsOfFollowees();					
//		ion.updateAuthorTweets();
		
		ion.identifyLinks();
	}

	private void identifyLinks() {
		// TODO Auto-generated method stub
		HashMap<String, Object> AlreadyCovered = new HashMap<String, Object>();
		Object dummy = new Object();
		HashMap<Long, Integer> AuthorOrder = new HashMap<Long, Integer>();
		int token_counter = 0;
		Long key = (long)-1;
		
		for(int i = 0; i < TweetInfo.size(); i++){
			if(this.rumor_type == 0){
				key = NameToID.get(TweetInfo.get(i).Author);
			} else if(this.rumor_type == 1){
				key = TweetInfo.get(i).Author_ID;
			}
			
			if(AuthorOrder.get(key) == null){
				AuthorOrder.put(key, token_counter);
				token_counter++;
			}
		}
		
		for(int i = 0; i < TweetInfo.size(); i++){
			String StringKey = "";
			Long key_buf = (long)-1;
			if(this.rumor_type == 0){
				StringKey = TweetInfo.get(i).Author;
			} else {
				key_buf = TweetInfo.get(i).Author_ID;
				StringKey = key_buf.toString();
			}
			if (AlreadyCovered.get(StringKey) != null)
				continue;
			
			HashMap<Long,Object> follow_rel = Followers.get(StringKey);
			HashMap<Long,Object> followee_rel = Followees.get(StringKey);
			
			key = (this.rumor_type == 0) ? NameToID.get(TweetInfo.get(i).Author) : key_buf;
			
			if(follow_rel != null){
				addLinks(key, follow_rel, true, AuthorOrder);
			}

			if(followee_rel != null){
				addLinks(key, follow_rel, false, AuthorOrder);
			}
			
			AlreadyCovered.put(StringKey, dummy);
		}
	}
	
	private void addLinks(Long node, HashMap<Long,Object> connections, boolean is_source, HashMap<Long,Integer> AuthorOrder){
		Iterator<Long> it = connections.keySet().iterator();
		Integer end1 = AuthorOrder.get(node);
		Integer end2 = null;
		
		while(it.hasNext()){
			Long connected_node = it.next();
			
			end2 = AuthorOrder.get(connected_node);
			if(end2 == null){
				continue;
			} else {
				if(is_source == true){
					dbm.updateLinkTable(end1, end2, this.rumor_type);	
				} else {
					dbm.updateLinkTable(end2, end1, this.rumor_type);
				}
			}
		}
	}

	public TweetNetwork(int rumor_type){
		dbm = new DBManager();
		this.rumor_type =rumor_type;  
		Followers = dbm.selectAllFollowing(true, rumor_type);
		Followees = dbm.selectAllFollowing(false, rumor_type);
	}
	
	private void updateAuthorTweets() {
		// TODO Auto-generated method stub
		HashMap<String,StringBuffer> at = new HashMap<String,StringBuffer>();
		int max_length = -1;
		
		for(int i = 0; i < TweetInfo.size(); i++){
			String key;
			TimeStamp it = TweetInfo.get(i);
			
			if(rumor_type == 0)
				key = it.Author;
			else
				key = Long.toString(it.Author_ID);
			
			if(at.get(key) == null){
				StringBuffer sb = new StringBuffer();
				sb.append(it.tweet_id);
				at.put(key, sb);
			} else {
				StringBuffer sb = at.get(key);
				sb.append(',');
				sb.append(it.tweet_id);
			}
		}
		
		for(int i = 0; i < TweetInfo.size(); i++){
			TimeStamp it = TweetInfo.get(i);
			String key;
			
			if(rumor_type == 0)
				key = it.Author;
			else
				key = Long.toString(it.Author_ID);
//			System.out.println("length - " + at.get(key).toString().length());
			if(at.get(key).toString().length() > max_length)
				max_length = at.get(key).toString().length();
			dbm.updateAuthorTweets(it.tweet_id, at.get(key).toString(), rumor_type);
		}
		
//		System.out.println("max length - " + max_length);
	}
	
	public void getTweets(){
		TweetInfo = dbm.selectTweetTimeline(-1, -1, rumor_type);
		NameToID = dbm.TweeterNameToID(rumor_type);
	}
	
	public void updateTweetsOfFollowers(){
		int max_length = -1;
		if(TweetInfo == null){
			System.out.println("error: load tweets first");
			System.exit(0);
		}
		
		for(int i = 0; i < TweetInfo.size(); i++){
			TimeStamp t = TweetInfo.get(i);
			
			HashMap<Long,Object> followers = null;
			if(rumor_type == 0)
				followers = Followers.get(t.Author);
			else
				followers = Followers.get(Long.toString(t.Author_ID));
			
			if(followers == null) continue;
			StringBuffer follower_string = new StringBuffer();
//			System.out.println("Searching " + Long.toString(t.Author_ID) + "'s followers");
			for(int j = i+1; j < TweetInfo.size(); j++){
				TimeStamp it = TweetInfo.get(j);
				if(rumor_type == 0){
					if(followers.get(NameToID.get(it.Author)) == null)
						continue;
				} else {
//					System.out.println("query key - " + it.Author_ID);
					if(followers.get(it.Author_ID) == null)
						continue;
				}
				follower_string.append(it.tweet_id);
				follower_string.append(',');
			}
			if(follower_string.length() > 0){
				follower_string.setLength(follower_string.length()-1);				
			}
//			System.out.println(follower_string.length());
			if(follower_string.length() > max_length)
				max_length = follower_string.length();
			dbm.updateFollowers(t.tweet_id, follower_string.toString(), rumor_type);
		}
		System.out.println("max length - " + max_length);
	}
	
	public void updateTweetsOfFollowees(){
		int max_length = -1;
		if(TweetInfo == null){
			System.out.println("error: load tweets first");
			System.exit(0);
		}
		
		for(int i = 1; i < TweetInfo.size(); i++){
			TimeStamp t = TweetInfo.get(i);

			if(rumor_type == 0)
				if(NameToID.get(t.Author) == null) continue;
			
			HashMap<Long,Object> followees = null; 
			
			if(rumor_type == 0)
				followees = Followees.get(NameToID.get(t.Author).toString());
			else
				followees = Followees.get(Long.toString(t.Author_ID));
			
			if(followees == null) continue;
			StringBuffer followee_string = new StringBuffer();
			
			for(int j = 0; j < i; j++){
				TimeStamp it = TweetInfo.get(j);
				
				if(rumor_type == 0){
					if(followees.get(NameToID.get(it.Author)) == null)
						continue;
				} else {
					if(followees.get(it.Author_ID) == null)
						continue;
				}
				
				followee_string.append(it.tweet_id);
				followee_string.append(',');
			}
			if(followee_string.length() > 0){
				followee_string.setLength(followee_string.length()-1);
			}
			if(followee_string.length() > max_length)
				max_length = followee_string.length();
//			System.out.println(followee_string.length());
			dbm.updateFollowees(t.tweet_id, followee_string.toString(), rumor_type);
		}
		System.out.println("max length - " + max_length);
	}
}
