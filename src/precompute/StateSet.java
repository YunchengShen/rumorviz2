package precompute;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

/**
 * A convenience class that wraps a pair of HashSets, which represent sets of active and passive users
 * @author Sam
 *
 */
public class StateSet<T> implements Iterable<HashSet<T>>
{
	public HashSet<T> active;
	public HashSet<T> passive;
	public ArrayList<HashSet<T>> both;
	
	public StateSet()
	{
		
		active = new HashSet<T>();
		passive = new HashSet<T>();
		
		//It's important to add passive, then active, because MultiStatePrecomputer is expecting
		//passive data to be associated with the value 0, and active with 1
		both = new ArrayList<HashSet<T>>();
		both.add(passive);
		both.add(active);
	}

	public boolean add(HashSet<T> e)
	{
		return both.add(e);
	}

	public HashSet<T> get(int index)
	{
		return both.get(index);
	}

	public Iterator<HashSet<T>> iterator()
	{
		return both.iterator();
	}
	
	
	
	

}
