package precompute;

public class InitState {
	public InitState(long date, int start, int rumorExp, int debunkExp,
			int bothExp, int rumorTwt, int debunkTwt, int bothTwt) {
		Date = date;
		Start = start;
		RumorExp = rumorExp;
		DebunkExp = debunkExp;
		BothExp = bothExp;
		RumorTwt = rumorTwt;
		DebunkTwt = debunkTwt;
		BothTwt = bothTwt;
	}

	public long Date;
	public int Start;
	public int RumorExp;
	public int DebunkExp;
	public int BothExp;
	public int RumorTwt;
	public int DebunkTwt;
	public int BothTwt;
}
