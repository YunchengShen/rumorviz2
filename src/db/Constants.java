package db;

public class Constants {
	public static boolean ec2_deploy_version = false;
	public static String host = (ec2_deploy_version == true) ? "127.0.0.1" : "resnick1.si.umich.edu";		
	public static String db = (ec2_deploy_version == true) ? "rumors" : "Rumors";
	public static String id = (ec2_deploy_version == true) ? "rumor" : "test";
	public static String pw = (ec2_deploy_version == true) ? "rumorinfo" : "newscube";

	public static String Meteor_Tweets = "meteor_rumors"; 			//meteor_rumor_tweets_seeds
	public static String Meteor_Followers = "meteor_followers";
	public static String Meteor_Followees = "meteor_followee";
	public static String Meteor_IStates = "meteor_initial_state_of_tweets";
	public static String Meteor_IStatesME = "meteor_initial_state_of_tweets_2";
	
	public static String Meteor_UStates = "meteor_state_update_of_tweets";
	public static String Meteor_UStatesME = "meteor_state_updates_multi_exp_2";
	
	public static String Meteor_Tweeters = "meteor_tweeter";
	public static String Meteor_TweetNet = "tweets_of_network";

	public static String Meteor_Multi_IStates = "meteor_initial_states_multi_exp_2";
	public static String Meteor_Multi_UStates = "meteor_state_updates_multi_exp_2";

	public static String Meteor_Multi_IStates_2 = "meteor_initial_states_multi_exp_2";
	public static String Meteor_Multi_UStates_2 = "meteor_state_updates_multi_exp_2";
	
	public static String Meteor_Seeds = "temp_meteor_seeds";
	public static String Meteor_trackbacks = "temp_meteor_trackbacks";
	
	public static String AP_IStates = "AP_initial_state";
	public static String AP_UStates = "AP_state_update";
	public static String AP_UStatesME = "AP_state_updates_multi_exp_2";
	public static String AP_Tweets = "AP_tweets";
	public static String AP_Tweeters = "AP_tweeter";
	public static String AP_Followers = "AP_follower";
	public static String AP_TweetNet = "AP_tweets_of_network";
	public static String AP_Followees = "AP_followee";
	
	
	public static String AP_Multi_IStates_2 = "AP_initial_states_multi_exp_2";
	public static String AP_Multi_UStates_2 = "AP_state_updates_multi_exp_2";
	
	public static String Tweeter_Relation = "tweeter_relation";
}
