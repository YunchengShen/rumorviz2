package db;

import java.io.BufferedReader;
import java.io.FileReader;

/**
 * This script takes manual annotations of tweets and rewrites those tweets in the DB to have those types. 
 * @author Sam
 *
 *	args[0]: path to the file that contains the annotations
 */
public class RewriteTweetTypesIntoDB
{
	
	public static void main(String[] args) throws Exception
	{
		BufferedReader reader = new BufferedReader(new FileReader(args[0]));
		DBManager manager = new DBManager();
		String line = null;
		String[] parts = null;
		Integer type = null;
		Integer id = null;
		
		while ((line = reader.readLine()) != null)
		{
			parts = line.split(",");
			type = Integer.parseInt(parts[0]);
			id = Integer.parseInt(parts[1]);
			manager.changeTypeOfAllThatMatchID(id,type,"AP_tweets");
		}
		
		reader.close();
	}

}
