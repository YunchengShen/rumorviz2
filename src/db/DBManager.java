package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import precompute.AuthorInfo;
import precompute.InitStateME;
import precompute.MultiStatePrecomputer;
import precompute.StateManager;
import precompute.StateManager.State;
import precompute.StateManager.UserType;
import precompute.StatePrecomputer;
import precompute.InitState;
import precompute.StateUpdate;
import precompute.StateUpdateME;
import precompute.TweeterLink;
import precompute.TweetsOfNetwork;
import java.sql.Types;

public class DBManager {
	private Connection con = null;
	private PreparedStatement ps = null;
	private Statement s = null;
	private ResultSet rs = null;

	public DBManager() {
		OpenConnect();
	}
	
	public void OpenConnect() {
		String url = "jdbc:mysql://" + Constants.host + ":3306/" + Constants.db;
		System.out.println("Connecting to " + Constants.host + " database " + Constants.db + " user name " + Constants.id);
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection(url, Constants.id, Constants.pw);
			ps = null;
			rs = null;
			s = null;
		} catch (ClassNotFoundException e) {
			System.out.println("Do not find driver");
		} catch (SQLException ex) {
			ex.printStackTrace();
			System.out.println("Uncomplete Connection");
		}
		if(con == null)
			System.out.println("connection failed");
//		System.out.println("logging anyway?");
		
		System.out.println("Testing connection...");
		testConnection();
	} // getConnection
	
	private void testConnection() 
	{
		try
		{
			String queryString = "SHOW TABLES";
			Statement s = con.createStatement();
			ResultSet rs = s.executeQuery(queryString);
			
			rs.beforeFirst();
			System.out.println("Tables:");
			while (rs.next())
			{
				System.out.println("\t"+rs.getString(1));
			}
			s.close();
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
		
	}

	public void initBuffer() {
		try
		{
			if (rs != null) {
				rs.close();
				rs = null;
			}
			if (ps != null) {
				ps.close();				
				ps = null;
			}
			if (s != null)
			{
				s.close();
				s = null;
			}
		}
		
		catch (Exception e)
		{
			System.out.println ( e);
		}
	}
	
	public ArrayList<Long> getTimeStampsInRange(long start, long end){
		if(start >= end) return null;
		
		String queryString="select distinct Date from " + Constants.Meteor_Tweets + " where Date > " + start + " and Date < " + end;
		ResultSet rs;
		ArrayList<Long> result = new ArrayList<Long>();
		
		try {
			Statement s = con.createStatement();
			
			rs = s.executeQuery(queryString);
			
			while(rs.next()){
				long timestamp = rs.getLong(1);
				result.add(timestamp);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	public ArrayList<String> selectSpreader(int start, int end, int rumor_type) {
		// TODO Auto-generated method stub
		Statement statement = null;
		ResultSet rs = null;
		String id, query, TableName;
		ArrayList<String> SpreaderList = new ArrayList<String>();
		Long a_id = (long) -1;
		
		if(rumor_type == 0)
			TableName = Constants.Meteor_Tweets;
		else 
			TableName = Constants.AP_Tweets;
		
		try {
			statement = con.createStatement();
			
			if(start == -1 && end == -1){
				query = "select Author from " + TableName + " where Type=1 group by Tweet_ID";
			} else {
				query = "select Author from " + TableName + " where Type=1 and Date >=" + start + " and Date <=" + end + " group by Tweet_ID";
			}
			rs = statement.executeQuery(query);
			
			while(rs.next()){
				if(rumor_type == 0){
					id = rs.getString(1);
					SpreaderList.add(id);
				} else {
					a_id = rs.getLong(1);
					SpreaderList.add(a_id.toString());
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SpreaderList;
	}
	
	public ArrayList<String> selectDebunker(int start, int end, int rumor_type) {
		// TODO Auto-generated method stub
		Statement statement = null;
		ResultSet rs = null;
		String id, query, TableName;
		ArrayList<String> DebunkerList = new ArrayList<String>();
		Long a_id = (long) -1;
		
		if(rumor_type == 0)
			TableName = Constants.Meteor_Tweets;
		else 
			TableName = Constants.AP_Tweets;
		
		try {
			statement = con.createStatement();
			
			if(start == -1 && end == -1){
				query = "select Author from " + TableName + " where Type=2 group by Tweet_ID";
			} else {
				query = "select Author from " + TableName + " where Type=2 and Date >=" + start + " and Date <=" + end + " group by Tweet_ID";
			}
			rs = statement.executeQuery(query);
			
			while(rs.next()){
				if(rumor_type == 0){
					id = rs.getString(1);
					DebunkerList.add(id);
				} else {
					a_id = rs.getLong(1);
					DebunkerList.add(a_id.toString());
				}
				
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return DebunkerList;
	}
	
	public HashMap<String,ArrayList<Long>> selectFollowersOfTSet(String TSet){
		Statement statement = null;
		ResultSet rs = null;		
		Long follower = (long) -1;
		String query, author;
		HashMap<String,ArrayList<Long>> Followers = new HashMap<String,ArrayList<Long>>();
		
		try {
			statement = con.createStatement();
			if(TSet.length() > 1)
				query = "select Author,Follower from " + Constants.Meteor_Followers + " where Author in (" + TSet + ")";
			else
				return null;
			System.out.println(query);
			rs = statement.executeQuery(query);
			
			while(rs.next()){
				author = rs.getString(1);
				follower = rs.getLong(2);
				if(Followers.get(author) == null){
					Followers.put(author,new ArrayList<Long>());
				} 
				(Followers.get(author)).add(follower);				
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return Followers;
	}
	
	public ArrayList<TimeStamp> selectTweetTimeline(int start, int end, int rumor_type) {
		// TODO Auto-generated method stub
		Statement statement = null;
		ResultSet rs = null;
		String name = "", query, tweet, TableName;
		long date, tweet_id, author_id = -1;
		int type;
		ArrayList<TimeStamp> SpreaderList = new ArrayList<TimeStamp>();
		if(rumor_type == 0)
			TableName = Constants.Meteor_Tweets;
		else 
			TableName = Constants.AP_Tweets;
		
		try {
			statement = con.createStatement();
			
			if(start == -1 && end == -1){
				query = "select Author, Date, Type, Tweet_ID, Text from " + TableName + " where Type=1 or Type=2 group by Tweet_ID order by Date asc";
			} else {
				query = "select Author, Date, Type, Tweet_ID, Text from " + TableName + " where Type=1 or Type=2 and Date >=" + start + " and Date <=" + end + " group by Tweet_ID order by Date asc";
			}
			rs = statement.executeQuery(query);
			
			while(rs.next()){
				if(rumor_type == 0){
					name = rs.getString(1);					
				} else {
					author_id = rs.getLong(1);					
				}
				date = rs.getLong(2);
				type = rs.getInt(3);
				tweet_id = rs.getLong(4);
				tweet = rs.getString(5);
				
				if(rumor_type == 0){
					SpreaderList.add(new TimeStamp(name, date, type, tweet_id, tweet));					
				} else {
					SpreaderList.add(new TimeStamp(author_id, date, type, tweet_id, tweet));				
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SpreaderList;
	}
	
	public HashMap<String,Long> TweeterNameToID(int rumor_type){
		Statement statement = null;
		ResultSet rs = null;
		String query, screen_name;
		long uid;
		
		HashMap<String,Long> Tweeters = new HashMap<String,Long>();
		
		try {
			statement = con.createStatement();
			if(rumor_type == 0){
				query = "select screen_name,uid from " + Constants.Meteor_Tweeters;				
			} else {
				query = "select AuthorName,Author from " + Constants.AP_Tweets;
			}
			
			rs = statement.executeQuery(query);
			
			while(rs.next()){
				screen_name = rs.getString(1);
				uid = rs.getLong(2);
				
				Tweeters.put(screen_name, uid);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Tweeters;
	}
	
	public HashMap<Long,String> TweeterIDToName(int rumor_type){
		Statement statement = null;
		ResultSet rs = null;
		String query, screen_name, TableName;
		long uid;
		
		HashMap<Long,String> Tweeters = new HashMap<Long, String>();
		
		if(rumor_type == 0)
			TableName = Constants.Meteor_Tweeters;
		else 
			TableName = Constants.AP_Tweeters;
		
		try {
			statement = con.createStatement();
			query = "select screen_name,uid from " + TableName;
			
			rs = statement.executeQuery(query);
			
			while(rs.next()){
				screen_name = rs.getString(1);
				uid = rs.getLong(2);
				Tweeters.put(uid, screen_name);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Tweeters;
	}

	public void updateInitState(int rumor_type, long tweet_id, long date, int start, int r_exp, int d_exp,
			int b_exp, int r_twt, int d_twt, int b_twt) {
		// TODO Auto-generated method stub
		PreparedStatement ps = null;
		String TableName;
		
		if(rumor_type == 0)
			TableName = Constants.Meteor_IStates;
		else 
			TableName = Constants.AP_IStates;
		
		String queryString="INSERT INTO "+ TableName +"(Tweet_ID, Date, Start, RumorExp, DebunkExp, BothExp, RumorTwt, DebunkTwt, BothTwt) VALUES ( ?,?,?,?,?,?,?,?,? )";
		int result;
		
		try {
			if(ps == null){
				ps = con.prepareStatement(queryString);
			}
			ps.setLong(1, tweet_id);
			ps.setLong(2, date);
			ps.setInt(3, start);
			ps.setInt(4,  r_exp);
			ps.setInt(5, d_exp);
			ps.setInt(6, b_exp);
			ps.setInt(7, r_twt);
			ps.setInt(8, d_twt);
			ps.setInt(9, b_twt);
			result = ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Version of updateInitState that accounts for the additional rumor and debunk exposure fields
	 * @param rumor_type
	 * @param tweet_id
	 * @param date
	 * @param start
	 * @param b_exp
	 * @param r_twt
	 * @param d_twt
	 * @param b_twt
	 * @param r_exp_1
	 * @param r_exp_2
	 * @param r_exp_3
	 * @param d_exp_1
	 * @param d_exp_2
	 * @param d_exp_3
	 */
	public void multiUpdateInitState(int rumor_type, long tweet_id, long date, int start,
			int b_exp, int r_twt, int d_twt, int b_twt, 
			int r_exp_1, int r_exp_2, int r_exp_3, 
			int d_exp_1, int d_exp_2, int d_exp_3, int user_type) {
		// TODO Auto-generated method stub
		PreparedStatement ps = null;
		String TableName;
		
		//We don't care about rumor type right now because we've only reconfigured the database to
		//handle the multiple exposure states variant for the meteor rumor
/*		if(rumor_type == 0)*/
			TableName = Constants.Meteor_Multi_IStates;
/*		else 
			TableName = Constants.AP_IStates;*/
		
		
		String queryString="INSERT INTO "+ TableName +"(Tweet_ID, Date, Start, BothExp, RumorTwt, DebunkTwt, BothTwt, ActiveFlag, " +
				"RumorExp1, RumorExp2, RumorExp3m, " +
				"DebunkExp1, DebunkExp2, DebunkExp3m) " +
				"VALUES ( ?,?,?,?,?,?,?,?,?,?,?,?,?,? )";
		int result;
		
		try {
			if(ps == null){
				ps = con.prepareStatement(queryString);
			}
			ps.setLong(1, tweet_id);
			ps.setLong(2, date);
			ps.setInt(3, start);
			ps.setInt(4, b_exp);
			ps.setInt(5, r_twt);
			ps.setInt(6, d_twt);
			ps.setInt(7, b_twt);
			ps.setInt(8, user_type);
			ps.setInt(9,  r_exp_1);
			ps.setInt(10,  r_exp_2);
			ps.setInt(11,  r_exp_3);
			ps.setInt(12, d_exp_1);
			ps.setInt(13, d_exp_2);
			ps.setInt(14, d_exp_3);
			result = ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Version of updateInitState that accounts for the additional rumor and debunk exposure fields
	 * @param rumor_type
	 * @param tweet_id
	 * @param date
	 * @param start
	 * @param b_exp
	 * @param r_twt
	 * @param d_twt
	 * @param b_twt
	 * @param r_exp_1
	 * @param r_exp_2
	 * @param r_exp_3
	 * @param d_exp_1
	 * @param d_exp_2
	 * @param d_exp_3
	 */
	public void multiUpdateInitState(int rumor_type, long tweet_id, long date, StateManager states, StateManager.UserType userType) 
	{
		PreparedStatement ps = null;
		String TableName;
		
		if (rumor_type == 0)
			TableName = Constants.Meteor_Multi_IStates_2;
		else
			TableName = Constants.AP_Multi_IStates_2;
		
/*		String queryString="INSERT INTO "+ TableName +"(Tweet_ID, Date, Start, BothExp, RumorTwt, DebunkTwt, BothTwt, ActiveFlag, " +
				"RumorExp1, RumorExp2, RumorExp3m, " +
				"DebunkExp1, DebunkExp2, DebunkExp3m) " +
				"VALUES ( ?,?,?,?,?,?,?,?,?,?,?,?,?,? )";*/
		StringBuilder columns = new StringBuilder("Tweet_ID, Date, ActiveFlag");
		StringBuilder qMarks = new StringBuilder("?,?,?");
		
		for (StateManager.State state : StateManager.State.values())
		{
			columns.append(", "+state.column);
			qMarks.append(",?");
		}
		
		String queryString = "INSERT INTO "+ TableName +"(" + columns.toString() + ") VALUES (" + qMarks.toString() + ")";
		
		int result;
		
		try {
			if(ps == null){
				ps = con.prepareStatement(queryString);
			}
			ps.setLong(1, tweet_id);
			ps.setLong(2, date);
			ps.setBoolean(3, userType.flag == 1);

			for (int i = 0; i < StateManager.State.values().length; i++)
			{
				ps.setInt(i+4, states.getCount(StateManager.State.values()[i],userType));
			}
			
			result = ps.executeUpdate();
		} catch (SQLException e) {
			
			
			e.printStackTrace();
		}
	}
	
	public ArrayList<InitState> getInitStates(int rumor_type){
		Statement statement = null;
		ResultSet rs = null;
		String query, TableName;
		ArrayList<InitState> iStates = new ArrayList<InitState>();
		
		if(rumor_type == 0)
			TableName = Constants.Meteor_IStates;
		else 
			TableName = Constants.AP_IStates;
		
		try {
			statement = con.createStatement();
			query = "select Date, Start, RumorExp, DebunkExp, BothExp, RumorTwt, DebunkTwt, BothTwt from " + 
					TableName + " order by Date asc";
			
			rs = statement.executeQuery(query);
			
			while(rs.next()){
				iStates.add(new InitState(rs.getLong(1), rs.getInt(2), rs.getInt(3), rs.getInt(4), rs.getInt(5), rs.getInt(6), rs.getInt(7), rs.getInt(8)));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return iStates;
	}

	public ArrayList<InitStateME> getInitStatesME(int rumor_type, boolean active){
		Statement statement = null;
		ResultSet rs = null;
		String query, TableName;
		ArrayList<InitStateME> iStates = new ArrayList<InitStateME>();
		
		if(rumor_type == 0)
			TableName = Constants.Meteor_Multi_IStates;
		else 
			TableName = Constants.AP_IStates;
		
		try {
			statement = con.createStatement();
			query = "select * from " + 
					TableName + " where ActiveFlag=" + active + " order by Date asc";
			
			rs = statement.executeQuery(query);
			
			while(rs.next()){
				InitStateME state_instance = new InitStateME();
				for(int i = 0; i < InitStateME.StateNames.length; i++){
					state_instance.setValue(i, rs.getInt(InitStateME.StateNames[i]));
				}
				iStates.add(state_instance);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return iStates;
	}
	
	public ArrayList<TweetsOfNetwork> getTweetNetwork(int rumor_type){
		Statement statement = null;
		ResultSet rs = null;
		String query, TableName;
		ArrayList<TweetsOfNetwork> TN = new ArrayList<TweetsOfNetwork>();
		
		if(rumor_type == 0)
			TableName = Constants.Meteor_TweetNet;
		else 
			TableName = Constants.AP_TweetNet;
		
		try {
			statement = con.createStatement();
			query = "select Tweets_of_Followers, Tweets_of_Followees, Tweets_of_Author from " + 
					TableName + " order by Date asc";
			
			rs = statement.executeQuery(query);
			
			while(rs.next()){
				TN.add(new TweetsOfNetwork(rs.getString(1), rs.getString(2), rs.getString(3)));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return TN;
	}
	
	public ArrayList<StateUpdate> getStateUpdate(int rumor_type){
		Statement statement = null;
		ResultSet rs = null;
		String query, TableName;
		ArrayList<StateUpdate> StateUpdates = new ArrayList<StateUpdate>();
		
		if(rumor_type == 0)
			TableName = Constants.Meteor_UStates;
		else 
			TableName = Constants.AP_UStates;
		
		try {
			statement = con.createStatement();
			query = "select Date, Start_RumorExp, Start_DebunkExp, Start_RumorTwt, Start_DebunkTwt, RumorExp_BothExp, " +
					"RumorExp_RumorTwt, RumorExp_DebunkTwt, DebunkExp_BothExp, DebunkExp_RumorTwt, DebunkExp_DebunkTwt, " +
					"BothExp_RumorTwt, BothExp_DebunkTwt, RumorTwt_BothTwt, DebunkTwt_BothTwt, RumorExp_RumorExp, " +
					"DebunkExp_DebunkExp, BothExp_BothExp, RumorTwt_RumorTwt, DebunkTwt_DebunkTwt, BothTwt_BothTwt from " + 
					TableName + " order by Date asc";
			
			rs = statement.executeQuery(query);
			
			while(rs.next()){
				StateUpdates.add(new StateUpdate(rs.getLong(1), rs.getInt(2), rs.getInt(3), rs.getInt(4), rs.getInt(5), 
						rs.getInt(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10), rs.getInt(11), rs.getInt(12), 
						rs.getInt(13), rs.getInt(14), rs.getInt(15), rs.getInt(16), rs.getInt(17), rs.getInt(18), rs.getInt(19),
						rs.getInt(20), rs.getInt(21)));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return StateUpdates;
	}
	
	public ArrayList<StateUpdateME> getStateUpdateME(int rumor_type, boolean active){
		Statement statement = null;
		ResultSet rs = null;
		String query, TableName;
		ArrayList<StateUpdateME> StateUpdates = new ArrayList<StateUpdateME>();
		
		if(rumor_type == 0)
			TableName = Constants.Meteor_UStatesME;
		else 
			TableName = Constants.AP_UStatesME;
		
		try {
			statement = con.createStatement();
			query = "select * from " + 
					TableName + " where ActiveFlag=" + active + " order by Date asc";
			
			rs = statement.executeQuery(query);
			
			while(rs.next()){
				StateUpdateME su_instance = new StateUpdateME();
				for(int i = 0; i < su_instance.transitionNames.length; i++){
					su_instance.setTransition(i, rs.getInt(su_instance.transitionNames[i]));
				}
				StateUpdates.add(su_instance);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return StateUpdates;
	}
	
	public HashMap<String,ArrayList<Long>> selectAllFollower(int rumor_type){
		Statement statement = null;
		ResultSet rs = null;
		String author = null, query, TableName;
		Long id = (long) -1, author_id = (long) -1;
		HashMap FollowerTable = null;
		FollowerTable = new HashMap<String,ArrayList<Long>>();
		
		try {
			statement = con.createStatement();
			
			if(rumor_type == 0){
				TableName = Constants.Meteor_Followers;
			} else {
				TableName = Constants.AP_Followers;
			}
			query = "select Author,Follower from " + TableName;								

			rs = statement.executeQuery(query);
			
			while(rs.next()){
				if(rumor_type == 0){
					author = rs.getString(1);					
				} else {
					author_id = rs.getLong(1);
				}
				id = rs.getLong(2);
				if(rumor_type == 0){
					if(FollowerTable.get(author) == null){
						FollowerTable.put(author,new ArrayList<Long>());
					} 
					((ArrayList<Long>) FollowerTable.get(author)).add(id);
				} else {
					if(FollowerTable.get(author_id.toString()) == null){
						FollowerTable.put(author_id.toString(), new ArrayList<Long>());
					} 
					((ArrayList<Long>) FollowerTable.get(author_id.toString())).add(id);
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return FollowerTable;
	}
	
	public HashMap<String,HashMap<Long,Object>> selectAllFollowing(boolean followers, int rumor_type){
		Statement statement = null;
		ResultSet rs = null;
		String author = null;
		Long id = (long) -1;
		String query, TableName = "";
		HashMap<String,HashMap<Long,Object>> FollowerTable = new HashMap<String,HashMap<Long,Object>>();
		
		if(rumor_type == 0 && followers == true){
			TableName = Constants.Meteor_Followers;
		} else if(rumor_type == 0 && followers == false){
			TableName = Constants.Meteor_Followees;
		} else if(rumor_type != 0 && followers == true){
			TableName = Constants.AP_Followers;
		} else if(rumor_type != 0 && followers == false){
			TableName = Constants.AP_Followees;
		}
		try {
			statement = con.createStatement();
			if(followers == true){
				query = "select Author,Follower from " + TableName;
			} else {
				query = "select Author,Followee from " + TableName;
			}
			rs = statement.executeQuery(query);
			Object dummy = new Object();
			
			while(rs.next()){
				if(rumor_type == 0){
					author = rs.getString(1);					
				} else {
					author = Long.toString(rs.getLong(1));
				}
				id = rs.getLong(2);
				if(FollowerTable.get(author) == null){
					FollowerTable.put(author,new HashMap<Long,Object>());
				}
				(FollowerTable.get(author)).put(id,dummy);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return FollowerTable;
	}
	
	public void updateStateUpdates(long tweet_id, long date, int[][] edgeWeight, int rumor_type) {
		// TODO Auto-generated method stub
		PreparedStatement ps = null;
		String TableName;
		
		if(rumor_type == 0)
			TableName = Constants.Meteor_UStates;
		else 
			TableName = Constants.AP_UStates;
		
		String queryString="INSERT INTO " + TableName +"(Tweet_ID, Date, Start_RumorExp, Start_DebunkExp, Start_RumorTwt, " +
				"Start_DebunkTwt, RumorExp_BothExp, RumorExp_RumorTwt, RumorExp_DebunkTwt, DebunkExp_BothExp, DebunkExp_RumorTwt, " +
				"DebunkExp_DebunkTwt, BothExp_RumorTwt, BothExp_DebunkTwt, RumorTwt_BothTwt, DebunkTwt_BothTwt) " +
				"VALUES ( ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? )";
		try {
			if(ps == null){
				ps = con.prepareStatement(queryString);
			}
			ps.setLong(1, tweet_id);
			ps.setLong(2, date);
			ps.setInt(3, edgeWeight[StatePrecomputer.start_id][StatePrecomputer.RExp_id]);
			ps.setInt(4, edgeWeight[StatePrecomputer.start_id][StatePrecomputer.DExp_id]);
			ps.setInt(5, edgeWeight[StatePrecomputer.start_id][StatePrecomputer.RTwt_id]);
			ps.setInt(6, edgeWeight[StatePrecomputer.start_id][StatePrecomputer.DTwt_id]);
			ps.setInt(7, edgeWeight[StatePrecomputer.RExp_id][StatePrecomputer.BExp_id]);
			ps.setInt(8, edgeWeight[StatePrecomputer.RExp_id][StatePrecomputer.RTwt_id]);
			ps.setInt(9, edgeWeight[StatePrecomputer.RExp_id][StatePrecomputer.DTwt_id]);
			ps.setInt(10, edgeWeight[StatePrecomputer.DExp_id][StatePrecomputer.BExp_id]);
			ps.setInt(11, edgeWeight[StatePrecomputer.DExp_id][StatePrecomputer.RTwt_id]);
			ps.setInt(12, edgeWeight[StatePrecomputer.DExp_id][StatePrecomputer.DTwt_id]);
			ps.setInt(13, edgeWeight[StatePrecomputer.BExp_id][StatePrecomputer.RTwt_id]);
			ps.setInt(14, edgeWeight[StatePrecomputer.BExp_id][StatePrecomputer.DTwt_id]);
			ps.setInt(15, edgeWeight[StatePrecomputer.RTwt_id][StatePrecomputer.BTwt_id]);
			ps.setInt(16, edgeWeight[StatePrecomputer.DTwt_id][StatePrecomputer.BTwt_id]);
			
			ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void multiUpdateStateUpdates(long tweet_id, long date, int[][] edgeWeight, int rumor_type, int userType) throws SQLException {
		PreparedStatement ps = null;
		String TableName;
		
		TableName = Constants.Meteor_Multi_UStates;
		
		String queryString="INSERT INTO " + TableName +"(Tweet_ID, Date, " +
				"Start_RumorExp1,   " +
				"Start_DebunkExp1, " +
				"Start_RumorTwt, Start_DebunkTwt, " +
				"RumorExp1_BothExp, RumorExp2_BothExp, RumorExp3m_BothExp, " +
				"RumorExp1_RumorTwt, RumorExp2_RumorTwt, RumorExp3m_RumorTwt, " +
				"RumorExp1_DebunkTwt, RumorExp2_DebunkTwt, RumorExp3m_DebunkTwt," +
				"DebunkExp1_BothExp, DebunkExp2_BothExp, DebunkExp3m_BothExp, " +
				"DebunkExp1_RumorTwt, DebunkExp2_RumorTwt, DebunkExp3m_RumorTwt, " +
				"DebunkExp1_DebunkTwt, DebunkExp2_DebunkTwt, DebunkExp3m_DebunkTwt, " +
				"BothExp_RumorTwt, BothExp_DebunkTwt, RumorTwt_BothTwt, DebunkTwt_BothTwt, " +
				"RumorExp1_RumorExp2, RumorExp2_RumorExp3m, " +
				"DebunkExp1_DebunkExp2, DebunkExp2_DebunkExp3m, " +
				"ActiveFlag) " +
				"VALUES ( ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? )";
		try {
			if(ps == null){
				ps = con.prepareStatement(queryString);
			}
			ps.setLong(1, tweet_id);
			ps.setLong(2, date);
			ps.setInt(3, edgeWeight[MultiStatePrecomputer.start_id][MultiStatePrecomputer.RExp_id_1]);
			ps.setInt(4, edgeWeight[MultiStatePrecomputer.start_id][MultiStatePrecomputer.DExp_id_1]);
			ps.setInt(5, edgeWeight[MultiStatePrecomputer.start_id][MultiStatePrecomputer.RTwt_id]);
			ps.setInt(6, edgeWeight[MultiStatePrecomputer.start_id][MultiStatePrecomputer.DTwt_id]);
			ps.setInt(7, edgeWeight[MultiStatePrecomputer.RExp_id_1][MultiStatePrecomputer.BExp_id]);
			ps.setInt(8, edgeWeight[MultiStatePrecomputer.RExp_id_2][MultiStatePrecomputer.BExp_id]);
			ps.setInt(9, edgeWeight[MultiStatePrecomputer.RExp_id_3][MultiStatePrecomputer.BExp_id]);
			ps.setInt(10, edgeWeight[MultiStatePrecomputer.RExp_id_1][MultiStatePrecomputer.RTwt_id]);
			ps.setInt(11, edgeWeight[MultiStatePrecomputer.RExp_id_2][MultiStatePrecomputer.RTwt_id]);
			ps.setInt(12, edgeWeight[MultiStatePrecomputer.RExp_id_3][MultiStatePrecomputer.RTwt_id]);
			ps.setInt(13, edgeWeight[MultiStatePrecomputer.RExp_id_1][MultiStatePrecomputer.DTwt_id]);
			ps.setInt(14, edgeWeight[MultiStatePrecomputer.RExp_id_2][MultiStatePrecomputer.DTwt_id]);
			ps.setInt(15, edgeWeight[MultiStatePrecomputer.RExp_id_3][MultiStatePrecomputer.DTwt_id]);
			ps.setInt(16, edgeWeight[MultiStatePrecomputer.DExp_id_1][MultiStatePrecomputer.BExp_id]);
			ps.setInt(17, edgeWeight[MultiStatePrecomputer.DExp_id_2][MultiStatePrecomputer.BExp_id]);
			ps.setInt(18, edgeWeight[MultiStatePrecomputer.DExp_id_3][MultiStatePrecomputer.BExp_id]);
			ps.setInt(19, edgeWeight[MultiStatePrecomputer.DExp_id_1][MultiStatePrecomputer.RTwt_id]);
			ps.setInt(20, edgeWeight[MultiStatePrecomputer.DExp_id_2][MultiStatePrecomputer.RTwt_id]);
			ps.setInt(21, edgeWeight[MultiStatePrecomputer.DExp_id_3][MultiStatePrecomputer.RTwt_id]);
			ps.setInt(22, edgeWeight[MultiStatePrecomputer.DExp_id_1][MultiStatePrecomputer.DTwt_id]);
			ps.setInt(23, edgeWeight[MultiStatePrecomputer.DExp_id_2][MultiStatePrecomputer.DTwt_id]);
			ps.setInt(24, edgeWeight[MultiStatePrecomputer.DExp_id_3][MultiStatePrecomputer.DTwt_id]);
			ps.setInt(25, edgeWeight[MultiStatePrecomputer.BExp_id][MultiStatePrecomputer.RTwt_id]);
			ps.setInt(26, edgeWeight[MultiStatePrecomputer.BExp_id][MultiStatePrecomputer.DTwt_id]);
			ps.setInt(27, edgeWeight[MultiStatePrecomputer.RTwt_id][MultiStatePrecomputer.BTwt_id]);
			ps.setInt(28, edgeWeight[MultiStatePrecomputer.DTwt_id][MultiStatePrecomputer.BTwt_id]);
			ps.setInt(29, edgeWeight[MultiStatePrecomputer.RExp_id_1][MultiStatePrecomputer.RExp_id_2]);
			ps.setInt(30, edgeWeight[MultiStatePrecomputer.RExp_id_2][MultiStatePrecomputer.RExp_id_3]);
			ps.setInt(31, edgeWeight[MultiStatePrecomputer.DExp_id_1][MultiStatePrecomputer.DExp_id_2]);
			ps.setInt(32, edgeWeight[MultiStatePrecomputer.DExp_id_2][MultiStatePrecomputer.DExp_id_3]);
			ps.setBoolean(33, (userType == 1)); //I believe tinyint and boolean are the same as far as MySQL are concerned
			ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			throw(e);
		}
	}
	
	public void multiUpdateStateUpdates(long tweet_id, long date, StateManager manager, int rumor_type, StateManager.UserType type)  {
		PreparedStatement ps = null;
		String TableName;
		
		if (rumor_type == 0)
			TableName = Constants.Meteor_Multi_UStates_2;
		else
			TableName = Constants.AP_Multi_UStates_2;

		
/*		String queryString="INSERT INTO " + TableName +"(Tweet_ID, Date, " +
				"Start_RumorExp1,   " +
				"Start_DebunkExp1, " +
				"Start_RumorTwt, Start_DebunkTwt, " +
				"RumorExp1_BothExp, RumorExp2_BothExp, RumorExp3m_BothExp, " +
				"RumorExp1_RumorTwt, RumorExp2_RumorTwt, RumorExp3m_RumorTwt, " +
				"RumorExp1_DebunkTwt, RumorExp2_DebunkTwt, RumorExp3m_DebunkTwt," +
				"DebunkExp1_BothExp, DebunkExp2_BothExp, DebunkExp3m_BothExp, " +
				"DebunkExp1_RumorTwt, DebunkExp2_RumorTwt, DebunkExp3m_RumorTwt, " +
				"DebunkExp1_DebunkTwt, DebunkExp2_DebunkTwt, DebunkExp3m_DebunkTwt, " +
				"BothExp_RumorTwt, BothExp_DebunkTwt, RumorTwt_BothTwt, DebunkTwt_BothTwt, " +
				"RumorExp1_RumorExp2, RumorExp2_RumorExp3m, " +
				"DebunkExp1_DebunkExp2, DebunkExp2_DebunkExp3m, " +
				"ActiveFlag) " +
				"VALUES ( ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? )";*/
		StringBuilder transitions = new StringBuilder("Tweet_ID, Date, ActiveFlag");
		StringBuilder qMarks = new StringBuilder("?,?,?");
		
		for (State source : manager.getMoveMap().get(type).keySet())
		{
			for (State dest: manager.getMoveMap().get(type).get(source).keySet())
			{
				transitions.append(", "+source.column+"_"+dest.column);
				qMarks.append(",?");
			}
		}
		
		String queryString = "INSERT INTO " + TableName + " ("+transitions.toString()+") VALUES (" + qMarks.toString() + ")";
		
		try {
			if(ps == null){
				ps = con.prepareStatement(queryString);
			}
			ps.setLong(1, tweet_id);
			ps.setLong(2, date);
			ps.setBoolean(3, type == UserType.active);
			
			int i = 4;
			for (State source : manager.getMoveMap().get(type).keySet())
			{
				for (State dest: manager.getMoveMap().get(type).get(source).keySet())
				{
					ps.setInt(i, manager.getMoveMap().get(type).get(source).get(dest));
					i++;
				}
			}

			ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void updateReflexiveUpdates(long tweet_id, int [][] edgeWeight, int rumor_type){
		String TableName;
		
		if(rumor_type == 0)
			TableName = Constants.Meteor_UStates;
		else 
			TableName = Constants.AP_UStates;
		
		String queryString = "UPDATE " + TableName + " SET RumorExp_RumorExp=?, DebunkExp_DebunkExp=?, BothExp_BothExp=?, " +
				"RumorTwt_RumorTwt=?, DebunkTwt_DebunkTwt=?, BothTwt_BothTwt=? WHERE Tweet_ID=?";
		
		try {
			if(ps == null){
				ps = con.prepareStatement(queryString);
			}
			
			ps.setInt(1, edgeWeight[StatePrecomputer.RExp_id][StatePrecomputer.RExp_id]);
			ps.setInt(2, edgeWeight[StatePrecomputer.DExp_id][StatePrecomputer.DExp_id]);
			ps.setInt(3, edgeWeight[StatePrecomputer.BExp_id][StatePrecomputer.BExp_id]);
			ps.setInt(4, edgeWeight[StatePrecomputer.RTwt_id][StatePrecomputer.RTwt_id]);
			ps.setInt(5, edgeWeight[StatePrecomputer.DTwt_id][StatePrecomputer.DTwt_id]);
			ps.setInt(6, edgeWeight[StatePrecomputer.BTwt_id][StatePrecomputer.BTwt_id]);
			ps.setLong(7, tweet_id);
			ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
	}
	

	
	
	public HashMap<String,AuthorInfo> selectAllAuthorInfo(int rumor_type){
		HashMap<String,AuthorInfo> AuthorInfoMap = new HashMap<String,AuthorInfo>();
		String TableName, query;
		
		if(rumor_type == 0)
			query = "select screen_name, followers, followees, RumorTweet, DebunkTweet, RumorExp, DebunkExp, uid from " + Constants.Meteor_Tweeters;
		else
			query = "select uid, followers, followees, RumorTweet, DebunkTweet, RumorExp, DebunkExp, screen_name from " + Constants.AP_Tweeters;
		
		
		Statement statement = null;
		ResultSet rs = null;
		
		String name, ID;
		int followers, followees, RumorTweet, DebunkTweet, RumorExp, DebunkExp;
		
		try {
			statement = con.createStatement();
			rs = statement.executeQuery(query);
			
			while(rs.next()){
				if(rumor_type == 0){
					name = rs.getString(1);					
				} else {
					name = Long.toString(rs.getLong(1));
				}
				followers = rs.getInt(2);
				followees = rs.getInt(3);
				RumorTweet = rs.getInt(4);
				DebunkTweet = rs.getInt(5);
				RumorExp = rs.getInt(6);
				DebunkExp = rs.getInt(7);
				if(rumor_type == 0){
					ID = Long.toString(rs.getLong(8));
				} else {
					ID = rs.getString(8);
				}
				
				AuthorInfoMap.put(name, new AuthorInfo(followers, followees, RumorTweet, DebunkTweet, RumorExp, DebunkExp, ID));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return AuthorInfoMap;
	}

	public void updateTweeterInfo(int rumor_type, String name, int re_count, int de_count, int tr_count, int td_count){
		String TableName;
		if(rumor_type == 0)
			TableName = Constants.Meteor_Tweeters;
		else 
			TableName = Constants.AP_Tweeters;
		
		String queryString = "UPDATE " + TableName + " SET RumorExp=?, DebunkExp=?, RumorTweet=?, DebunkTweet=? WHERE screen_name=?";
		PreparedStatement ps = null;
		int result;
		try {
			if(ps == null){
				ps = con.prepareStatement(queryString);
			}
			if(re_count != -1)
				ps.setInt(1, re_count);
			else
				ps.setNull(1, Types.INTEGER);
			
			if(de_count != -1)
				ps.setInt(2, de_count);
			else
				ps.setNull(2, Types.INTEGER);
			
			if(tr_count != -1)
				ps.setInt(3, tr_count);
			else
				ps.setNull(3, Types.INTEGER);
			
			if(td_count != -1)
				ps.setInt(4, td_count);
			else 
				ps.setNull(4, Types.INTEGER);
			
			ps.setString(5, name);
			
//			System.out.println("RE = " + re_count + ", DE = " + de_count + ", TR = " + tr_count + ", TD = " + td_count);
//			System.out.println(ps);
			result = ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void updateFollowers(long tweet_id, String followers, int rumor_type) {
		// TODO Auto-generated method stub
		PreparedStatement ps = null;
		String TableName = "";
		if(rumor_type == 0){
			TableName = Constants.Meteor_TweetNet;
		} else {
			TableName = Constants.AP_TweetNet;
		}
		String queryString="UPDATE " + TableName + " SET Tweets_of_Followers=? WHERE Tweet_ID=?";
		int result;
		try {
			if(ps == null){
				ps = con.prepareStatement(queryString);
			}
			ps.setString(1, followers);
			ps.setLong(2, tweet_id);
			
			result = ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void updateFollowees(long tweet_id, String followees, int rumor_type) {
		// TODO Auto-generated method stub
		PreparedStatement ps = null;
		String TableName = "";
		if(rumor_type == 0){
			TableName = Constants.Meteor_TweetNet;
		} else {
			TableName = Constants.AP_TweetNet;
		}
		String queryString="UPDATE " + TableName + " SET Tweets_of_Followees=? WHERE Tweet_ID=?";
		int result;
		try {
			if(ps == null){
				ps = con.prepareStatement(queryString);
			}
			ps.setString(1, followees);
			ps.setLong(2, tweet_id);
			
			result = ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void updateAuthorTweets(long tweet_id, String at, int rumor_type) {
		PreparedStatement ps = null;
		String TableName;
		if(rumor_type == 0)
			TableName = Constants.Meteor_TweetNet;
		else 
			TableName = Constants.AP_TweetNet;
		
		String queryString="UPDATE " + TableName + " SET Tweets_of_Author=? WHERE Tweet_ID=?";
		int result;
		try {
			if(ps == null){
				ps = con.prepareStatement(queryString);
			}
			ps.setString(1, at);
			ps.setLong(2, tweet_id);
			
			result = ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void updateText(long tweet_id, String text) {
		PreparedStatement ps = null;
		String queryString = "UPDATE " + Constants.Meteor_Seeds + " SET Text=? WHERE Tweet_ID=?";
		String queryString2 = "UPDATE " + Constants.Meteor_trackbacks + " SET Text=? WHERE Tweet_ID=?";
		int result;
		try {
			if(ps == null){
				ps = con.prepareStatement(queryString);
			}
			ps.setString(1, text);
			ps.setLong(2, tweet_id);
			result = ps.executeUpdate();
			
			ps = con.prepareStatement(queryString2);
			ps.setString(1, text);
			ps.setLong(2, tweet_id);
			result = ps.executeUpdate();			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public HashMap<Long,String> selectAllEmbed(int rumor_id){
		String TableName, query;
		HashMap<Long,String> EmbedHTML = new HashMap<Long,String>();
		
		if(rumor_id == 0)
			TableName = Constants.Meteor_Tweets;
		else
			TableName = Constants.AP_Tweets;
		
		query = "SELECT Tweet_ID, Embed FROM " + TableName;
		Statement statement;
		try {
			statement = con.createStatement();
			rs = statement.executeQuery(query);
			
			while(rs.next()){
				long id = rs.getLong(1);
				String HTML = rs.getString(2);
				
				if(HTML == null)
					continue;
				else
					EmbedHTML.put(id, HTML);
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
		
		return EmbedHTML;
	}

	public void updateLinkTable(Integer source, Integer target, int rumor_type) {
		// TODO Auto-generated method stub
		PreparedStatement ps = null;
		String TableName = Constants.Tweeter_Relation;
		
		String queryString="INSERT INTO "+ TableName +"(RumorID,Source,Target) VALUES ( ?,?,? )";
		int result;
		
		try {
			if(ps == null){
				ps = con.prepareStatement(queryString);
			}
			ps.setLong(1, rumor_type);
			ps.setLong(2, source);
			ps.setInt(3, target);
			result = ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public ArrayList<TweeterLink> selectTweetLinks(int rumor_type){
		String TableName = Constants.Tweeter_Relation;
		String query;
		
		ArrayList<TweeterLink> links = new ArrayList<TweeterLink>();
		query = "SELECT Source,Target FROM " + TableName + " WHERE RumorID=" + rumor_type;
		Statement statement;
		try {
			statement = con.createStatement();
			rs = statement.executeQuery(query);
			
			while(rs.next()){
				int s = rs.getInt(1);
				int t = rs.getInt(2);
				
				links.add(new TweeterLink(s, t));
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
		
		return links;
	}

	public void changeTypeOfAllThatMatchID(Integer id, Integer type, String tableName) throws Exception
	{
		
		String query = "UPDATE "+tableName+" t1 JOIN "+tableName+" t2 SET t1.Type = " + type + " WHERE t2.id = "
		+ id + " AND t1.Text = t2.Text";
		Statement statement = con.createStatement();
		statement.execute(query);
		statement.close();
		
	}
}
