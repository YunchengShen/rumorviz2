package db;

public class TimeStamp {

	public long date;
	public String Author;
	public long Author_ID;
	public int type;
	public long tweet_id;
	public String Tweet;
	
	public TimeStamp(String Author, long date, int type, long tweet_id, String tweet) {
		// TODO Auto-generated constructor stub
		this.Author = Author;
		this.date = date;
		this.type = type;
		this.tweet_id = tweet_id;
		this.Tweet = tweet;
	}
	
	public TimeStamp(Long Author, long date, int type, long tweet_id, String tweet) {
		// TODO Auto-generated constructor stub
		this.Author_ID = Author;
		this.date = date;
		this.type = type;
		this.tweet_id = tweet_id;
		this.Tweet = tweet;
	}
}
